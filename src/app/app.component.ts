import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { File } from '@ionic-native/file';
import { General } from '../classes/general';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = 'LoginPage';

  constructor(
    public platform: Platform, 
    public splashScreen: SplashScreen,
    private file: File
    ) {
    this.platform.ready().then(() => {
      setTimeout(() =>{
        this.splashScreen.hide();
        General.dataDirectory = this.file.dataDirectory;
      }, 100);
    });
  }
}

