// Modules
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SQLite } from '@ionic-native/sqlite';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireModule } from 'angularfire2';
import { HttpModule } from '@angular/http';
import { FileTransfer } from '@ionic-native/file-transfer';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { IonicStorageModule } from '@ionic/storage';
import { AngularFireStorageModule } from 'angularfire2/storage';

// Providers
import { DatabaseProvider } from '../providers/database/database';
import { LoadingProvider } from '../providers/loading/loading';
import { AuthProvider } from '../providers/auth/auth';
import { HelperProvider } from '../providers/helper/helper';

// Components
import { MyApp } from './app.component';
import { ExamProvider } from '../providers/exam/exam';
import { FileProvider } from '../providers/file/file';
import { MediaProvider } from '../providers/media/media';

import 'rxjs/add/operator/take';

var firebaseConfig = {
  apiKey: "AIzaSyCSasFDCYyMKXxWwqXvfQtP-r8EaTHXY0c",
  authDomain: "knowfoot-6969d.firebaseapp.com",
  databaseURL: "https://knowfoot-6969d.firebaseio.com",
  projectId: "knowfoot-6969d",
  storageBucket: "knowfoot-6969d.appspot.com",
  messagingSenderId: "203136955084"
};

@NgModule({
  declarations: [
    MyApp,
  ],    
  imports: [
    BrowserModule,
    HttpModule,
    AngularFireDatabaseModule,
    AngularFirestoreModule,
    AngularFirestoreModule.enablePersistence(),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireStorageModule,
    AngularFireAuthModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [MyApp],
  providers: [
    StatusBar,
    DatabaseProvider,
    Camera,
    FileTransfer,
    File, 
    SQLite,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    LoadingProvider,
    SplashScreen,
    AuthProvider,
    HelperProvider,
    ExamProvider,
    FileProvider,
    MediaProvider
  ]
})
export class AppModule {}
