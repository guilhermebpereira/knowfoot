export class FpiImagens {
    static conjuntoImagensDireito: string[][] = [
        ['assets/imgs/direito-maleolo-2.png','assets/imgs/direito-maleolo-1.png','assets/imgs/direito-maleolo.png','assets/imgs/direito-maleolo1.png','assets/imgs/direito-maleolo2.png', ],
        ['assets/imgs/direito-calcaneo-2.png', 'assets/imgs/direito-calcaneo-1.png', 'assets/imgs/direito-calcaneo.png', 'assets/imgs/direito-calcaneo1.png', 'assets/imgs/direito-calcaneo2.png'],
        ['assets/imgs/direito-abducao-2.png', 'assets/imgs/direito-abducao-1.png', 'assets/imgs/direito-abducao.png', 'assets/imgs/direito-abducao1.png', 'assets/imgs/direito-abducao2.png'],
        ['assets/imgs/direito-arco-2.png', 'assets/imgs/direito-arco-1.png', 'assets/imgs/direito-arco.png', 'assets/imgs/direito-arco1.png', 'assets/imgs/direito-arco2.png'],
        ['assets/imgs/direito-arco-2.png', 'assets/imgs/direito-arco-1.png', 'assets/imgs/direito-arco.png', 'assets/imgs/direito-arco1.png', 'assets/imgs/direito-arco2.png'],
    ];
    
    static conjuntoImagensEsquerdo: string[][] = [
        ['assets/imgs/esquerdo-maleolo-2.png','assets/imgs/esquerdo-maleolo-1.png','assets/imgs/esquerdo-maleolo.png','assets/imgs/esquerdo-maleolo1.png','assets/imgs/esquerdo-maleolo2.png', ],
        ['assets/imgs/esquerdo-calcaneo-2.png', 'assets/imgs/esquerdo-calcaneo-1.png', 'assets/imgs/esquerdo-calcaneo.png', 'assets/imgs/esquerdo-calcaneo1.png', 'assets/imgs/esquerdo-calcaneo2.png'],
        ['assets/imgs/esquerdo-abducao-2.png', 'assets/imgs/esquerdo-abducao-1.png', 'assets/imgs/esquerdo-abducao.png', 'assets/imgs/esquerdo-abducao1.png', 'assets/imgs/esquerdo-abducao2.png'],
        ['assets/imgs/esquerdo-arco-2.png', 'assets/imgs/esquerdo-arco-1.png', 'assets/imgs/esquerdo-arco.png', 'assets/imgs/esquerdo-arco1.png', 'assets/imgs/esquerdo-arco2.png'],
        ['assets/imgs/esquerdo-arco-2.png', 'assets/imgs/esquerdo-arco-1.png', 'assets/imgs/esquerdo-arco.png', 'assets/imgs/esquerdo-arco1.png', 'assets/imgs/esquerdo-arco2.png'],
    ];
}