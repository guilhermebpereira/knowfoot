export class Observar{
    static posicao = 
    [
        "Curvaturas acima e abaixo do maléolo lateral", 
        "Posição do calcâneo", 
        "Abdução e Adução do antepé sobre o retropé", 
        "Altura do arco longitudinal medial",
        "Proeminência na região da articulação talonavicular (ATN)"
    ];
    
    static textos = 
    [
        // Posição 1
        {
            texto: 
            [
                "Acima do maléolo lateral apresenta uma curvatura acentuada e abaixo uma linha reta",
                "Acima do maléolo lateral apresenta uma pequena curvatura e abaixo uma linha reta",
                "As curvaturas acima e abaixo do maléolo lateral  são semelhantes",
                "A curvatura acima do maléolo lateral está quase retificada e abaixo apresenta uma pequena curvatura",
                "A curvatura acima do maléolo lateral se apresenta retificada e abaixo uma curvatura acentuada."
            ]
        },
        // Posição 2
        {
            texto: 
            [
                "A partir do ângulo de 90°, quando há um desvio maior que 5 GRAUS de inversão do calcâneo para a Direita",
                "A partir do ângulo de 90°, desvio levemente invertido (ATÉ 5 GRAUS) do calcâneo à Direita",
                "Ângulo de 90°",
                "A partir do ângulo de 90°, desvio levemente evertido (ATÉ 5 GRAUS) do calcâneo à Esquerda",
                "A partir do ângulo de 90°, quando há um desvio maior que 5 GRAUS de eversão do calcâneo à Esquerda"
            ]
            
        },
        // Posição 3
        {
            texto: 
            [
                "São observados mais de dois dedos na vista medial",
                "Apenas um dedo é observado na vista medial",
                "Observa-se a mesma quantidade de dedos mediais e laterais",
                "Um ou dois dedos observados na vista lateral",
                "São observados mais de dois dedos na vista lateral e nenhum na vista medial"
            ]
        },
        // Posição 4
        {
            texto: 
            [
                "Arco longitudinal medial muito elevado e dedos em garra (leve)",
                "Arco longitudinal medial elevado",
                "Arco longitudinal medial neutro (altura intermediária entre – 1 e +1)",
                "Arco longitudinal medial pouco achatado (quase encostado no chão)",
                "Arco longitudinal medial muito achatado (encostado no chão)"
            ]
        },
        // Posição 5
        {
            texto: 
            [
                "Região ATN  apresenta concavidade profunda",
                "Região ATN  apresenta concavidade menos profunda",
                "Região ATN  retificada (neutra)",
                "Região ATN  apresenta convexidade menos profunda",
                "Região ATN  apresenta convexidade profunda"
            ]
        },
    ];
}