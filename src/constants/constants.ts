export abstract class Constants {
    public static auth = {
        WRONG_PASSWORD: "auth/wrong-password",
        USER_NOT_FOUND: "auth/user-not-found",
        USER_DISABLED: "auth/user-disabled",
        INVALID_EMAIL: "auth/invalid-email",
        EMAIL_ALREADY_IN_USE: "auth/email-already-in-use",
        WEAK_PASSWORD: "auth/weak-password"
    };
}