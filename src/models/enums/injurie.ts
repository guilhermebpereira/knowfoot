export enum Injurie {
  AnkleSprain = 'Entorse de Tornozelo',
  PlantarFasciitis = 'Fascite plantar',
  CalcaneanSpur = 'Esporão de Calcâneo',
  Metatarsalgia = 'Metatarsalgia',
  Another = 'Outro(s)'
};
