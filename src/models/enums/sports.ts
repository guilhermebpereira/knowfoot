export enum Sport {
  Soccer = "Futebol",
  Running = "Corrida",
  Triathlon = "Triathlon",
  Cycling = "Ciclismo",
  Swimming = "Natação",
  Basketball = "Basquete",
  MartialArts = "Artes Marciais",
  Volleyball = "Vôlei",
  Another = "Outro(s)"
};

export enum SportPraticeTime {
  LessThanSixMonths = "Menos de 6 meses",
  BetweenSixAndTwentyFourMonths = "Entre 6 e 24 meses",
  MoreThanTwoYears = "Mais de 2 anos" 
};

export enum SportPraticeFrequency {
  OnePerWeek = "1 vez por semana",
  TwoOrTreePerWeek = "2 a 3 vezes por semana",
  MoreThanThreePerWeek = "Mais de 3 vezes por semana"
};
