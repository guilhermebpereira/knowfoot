export interface Drop {
  pe: string;
  medidaSentada: number;
  medidaEmPe: number;
  valorResultado: number;
  resultado: string;
  dataDrop: string;
  dataDrop2: number;
};

export interface DropWithId extends Drop {
  id: string;
}