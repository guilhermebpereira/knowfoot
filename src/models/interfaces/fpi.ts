import { MediaFile } from './media-file';

export class Fpi {
  parecida: number[] = [];
  vl_parecida: number[] = [];
  nm_image: string[] = [];
  dt_fpi: string;
  dt_fpi2: number;
  nm_resultado: string;
  ic_pe: string;
  cd_paciente: number;
  medias?: MediaFile[];
};

export interface FpiWithId extends Fpi {
  id: string;
};

