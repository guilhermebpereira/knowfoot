import { FileType } from "../enums/file-type";

export interface MediaFile {
  fileName: string;
  fileType: FileType;
  downloadURL?: string;
};

