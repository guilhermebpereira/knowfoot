import { FootSide } from "../enums/foot-side";
import { Sport, SportPraticeTime, SportPraticeFrequency } from "../enums/sports";
import { Injurie } from "../enums/injurie";

export interface Patient {
  name: string;
  height: number;
  birthYear: number;
  weight: number;
  gender: string;
  dominantFoot: FootSide;
  sport: Sport;
  leftFootInjuries: Injurie[] | string;
  rightFootInjuries: Injurie[] | string;
  sportPraticeTime: SportPraticeTime;
  sportPraticeFrequency: SportPraticeFrequency;
  isDeleted: boolean;
  registrationTimestamp?: any;
}

export interface PatientWithId extends Patient {
  id: string;
}
