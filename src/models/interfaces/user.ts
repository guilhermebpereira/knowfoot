export enum Occupation {
    doctor = "Médico",
    physiotherapist = "Fisioterapeuta",
    physicalEducator = "Educador Físico",
    salesMan = "Vendedor(a)",
    healthAreaStudent = "Estudante(área da saúde)",
    student = "Estudante"
}

export enum EduccationLevel {
    elementarySchool = "Fundamental",
    highSchool = "Médio",
    higherEducation = "Superior",
    higherEducationHealthArea = "Superior(área da saúde)",
    postGraduate = "Pós-Graduação",
    postGraduteHealthArea = "Pós-Graduação(área da saúde)"
}

export interface User {
    name: string;
    birthYear: number;
    occupation: Occupation;
    educcationLevel: EduccationLevel;
}