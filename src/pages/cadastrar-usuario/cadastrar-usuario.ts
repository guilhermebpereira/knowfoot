import { Component } from '@angular/core';
import { IonicPage, NavController, Alert, AlertController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { CustomValidators } from '../../validators/CustomValidators';
import { HelperProvider } from '../../providers/helper/helper';
import { AngularFirestore } from 'angularfire2/firestore';
import { Occupation, EduccationLevel, User } from '../../models/interfaces/user';
import { LoadingProvider } from '../../providers/loading/loading';
import { Constants } from '../../constants/constants';
import { KeyValuePair } from '../../models/interfaces/key-value-pair';
import { AuthProvider } from '../../providers/auth/auth';

@IonicPage()
@Component({
    selector: 'page-cadastrar-usuario',
    templateUrl: 'cadastrar-usuario.html',
})
export class CadastrarUsuarioPage {
    form: FormGroup;
    user: User = {} as User;
    occupationOptions: KeyValuePair[] = [];
    educcationLevelOptions: KeyValuePair[] = [];

    constructor(
        private navCtrl: NavController,
        private formBuilder: FormBuilder,
        private helperProvider: HelperProvider,
        private angularFirestore: AngularFirestore,
        private loadingProvider: LoadingProvider,
        private alertController: AlertController,
        private authProvider: AuthProvider
    ) {
        this.form = this.createForm();  
        this.occupationOptions = this.helperProvider.translateEnum(Occupation);
        this.educcationLevelOptions = this.helperProvider.translateEnum(EduccationLevel);
    }

    private createForm(): FormGroup {
        const form: FormGroup = this.formBuilder.group({
            name: ['', Validators.compose([Validators.required, Validators.maxLength(55)])],
            birthYear: ['', Validators.compose([Validators.required, Validators.min(1910), Validators.max(2003)])],
            occupation: ['', Validators.compose([Validators.required, Validators.maxLength(45)])],
            edducationLevel: ['', Validators.compose([Validators.required, Validators.maxLength(25)])],
            email: ['', Validators.compose([Validators.email, Validators.required])],
            password: ['', Validators.required],
            confirmPassword: ['', Validators.compose([Validators.required, CustomValidators.mirror("password", "As senhas não conferem")])]
        });

        return form;
    }

    registerUser(form: FormGroup, user: User): void {
        (async () => {
            const email: string = this.form.get("email").value;
            const password: string = this.form.get("password").value;
            let callback: () => void = () => {
                this.loadingProvider.hidePreloader();
                form.reset();
            };

            this.loadingProvider.displayPreloader("Cadastrando...");

            const userUid: string | void = await this.authProvider.createUserWithEmailAndPassword(email, password)
                .catch((error) => {
                    if (error && error.code == Constants.auth.WEAK_PASSWORD) {
                        callback = () => {
                            this.loadingProvider.hidePreloader();
                            form.patchValue({
                                password: "",
                                confirmPassword: ""
                            });
                        };
                    }
                    this.handleRegisterUserError(error.code, callback)
                });

            if (userUid) {
                let hasError: boolean = false;

                await this.angularFirestore.doc(`users/${userUid}`).set({
                    name: user.name,
                    birthYear: user.birthYear,
                    occcupation: user.occupation,
                    edducationLevel: user.educcationLevel
                }).catch((error) => {
                    hasError = true;
                    this.handleRegisterUserError(error ? error.code : '', callback)
                });

                if (!hasError) {
                    this.loadingProvider.hidePreloader();
                    this.handleRegisterUserSuccess();
                }
            }
        })();
    }

    private handleRegisterUserSuccess(): void {
        const alert: Alert = this.alertController.create({
            title: "Sucesso",
            message: "Cadastro efetuado com sucesso.",
            buttons: [
                {
                    role: "cancel",
                    text: "Ok",
                    handler: () => {
                        this.form.reset();
                        this.navCtrl.setRoot("HomePage");
                    }
                }
            ]        
        });

        alert.present();
    }

    private handleRegisterUserError(error: string, callback: () => void) {
        let message: string;
        
        switch (error) {
            case Constants.auth.EMAIL_ALREADY_IN_USE:
                message = "Email inserido já está em uso";
                break;
            case Constants.auth.INVALID_EMAIL:
                message = "Email inválido";
                break;
            case Constants.auth.WEAK_PASSWORD:
                message = "Senha fraca, insira uma senha mais forte";
                break;
            default:
                message = "Não foi possível concluir o cadastro";
                break;
        }

        const alert: Alert = this.alertController.create({
            title: "Erro",
            "message": message,
            buttons: [
                {
                    text: "Ok",
                    role: "cancel",
                    handler: callback
                }
            ]
        });

        alert.present();
    }
}
