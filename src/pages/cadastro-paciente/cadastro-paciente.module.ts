import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CadastroPacientePage } from './cadastro-paciente';
import { PatientProvider } from "../../providers/patient/patient";
import { ExamProvider } from '../../providers/exam/exam';

@NgModule({
  declarations: [CadastroPacientePage],
  imports: [IonicPageModule.forChild(CadastroPacientePage)],
  exports: [CadastroPacientePage],
  providers: [
    PatientProvider,
    ExamProvider
  ]
})
export class CadastroPacientePageModule {}
