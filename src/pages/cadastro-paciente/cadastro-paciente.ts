import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Select, ViewController, Form } from 'ionic-angular';
import { AlertController, ToastController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl, AbstractControl } from '@angular/forms';
import { PatientProvider } from '../../providers/patient/patient';
import { LoadingProvider } from '../../providers/loading/loading';
import { File } from '@ionic-native/file';
import { StorageReference } from 'firebase/storage';
import { IonicPage, Alert } from 'ionic-angular';
import firebase from 'firebase';
import { Patient } from '../../models/interfaces/patient';
import { Gender } from '../../models/enums/gender';
import { FootSide } from '../../models/enums/foot-side';
import { Injurie } from '../../models/enums/injurie';
import { Sport, SportPraticeTime, SportPraticeFrequency } from '../../models/enums/sports';
import { AuthProvider } from '../../providers/auth/auth';
import { ExamType } from '../../models/enums/exam-type';
import { KeyValuePair } from '../../models/interfaces/key-value-pair';
import { HelperProvider } from '../../providers/helper/helper';
import { ExamProvider } from '../../providers/exam/exam';
import { Drop } from '../../models/interfaces/drop';
import { Fpi } from '../../models/interfaces/fpi';

declare var cordova: any;

interface RegisterPatientFormValue {
    name: string;
    height: number;
    birthYear: number;
    weight: number;
    gender: Gender;
    dominantFoot: FootSide;
    leftFootInjuries: Injurie[];
    rightFootInjuries: Injurie[];
    sport: Sport;
    sportPraticeTime: SportPraticeTime;
    sportPraticeFrequency: SportPraticeFrequency;
};

@IonicPage()
@Component({
    selector: 'page-cadastro-paciente',
    templateUrl: 'cadastro-paciente.html',
})
export class CadastroPacientePage {
    @ViewChild('leftSelect') leftSelect: Select;
    @ViewChild('rightSelect') rightSelect: Select;

    private userKey: string;
    private newPacienteKey: string
    private examType: ExamType;
    doesPraticeSport: boolean = true; 
    avaliarOutro: string;
    imagesDownloadUrls: string[] = [];
    drop: Drop;
    fpi: Fpi;
    form: FormGroup;
    patient: Patient = {} as Patient;
    resultado: string;
    sports: KeyValuePair[] = [];
    injuries: KeyValuePair[] = [];
    footSides: KeyValuePair[] = [];
    sportPraticeTimeOptions: KeyValuePair[] = [];
    sportPraticeFrequencyOptions: KeyValuePair[] = [];
    genderOptions: KeyValuePair[] = [];
    hasLeftFootInjuries: boolean = true;
    hasRightFootInjuries: boolean = true;
    footSideEnum = FootSide;

    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        private alertCtrl: AlertController,
        private patientProvider: PatientProvider,
        private loadingProvider: LoadingProvider,
        private authProvider: AuthProvider,
        private helperProvider: HelperProvider,
        private examProvider: ExamProvider
    ) {
        this.imagesDownloadUrls = [];
        this.avaliarOutro = null;
        this.userKey = this.authProvider.currentUserUid;
        this.examType = this.navParams.get("examType");
        this.sports = this.helperProvider.translateEnum(Sport);
        this.injuries = this.helperProvider.translateEnum(Injurie);
        this.footSides = this.helperProvider.translateEnum(FootSide);
        this.sportPraticeTimeOptions = this.helperProvider.translateEnum(SportPraticeTime);
        this.sportPraticeFrequencyOptions = this.helperProvider.translateEnum(SportPraticeFrequency);
        this.genderOptions = this.helperProvider.translateEnum(Gender);

        if (this.examType === ExamType.FPI || this.examType === ExamType.Drop) {
            if (this.navParams.get("fpi") != null) {
                this.fpi = this.navParams.get("fpi");
            }
            else if (this.navParams.get("drop") != null) {
                this.drop = this.navParams.get("drop");
            }
        }
        else {
            console.error("sem objeto fpi ou drop");
            this.navCtrl.pop();
        }

        this.instantiateForm();
    }

    private instantiateForm(): void {
        this.form = new FormGroup({
            name: new FormControl("", Validators.compose([Validators.required, Validators.maxLength(55)])),
            height: new FormControl("", Validators.compose([Validators.required, Validators.min(50), Validators.max(300)])),
            birthYear: new FormControl("", Validators.compose([Validators.required, Validators.min(1910), Validators.max(2018)])),
            weight: new FormControl("", Validators.required),
            gender: new FormControl("", Validators.required),
            dominantFoot: new FormControl("", Validators.required),
            leftFootInjuries: new FormControl("", Validators.required),
            rightFootInjuries: new FormControl("", Validators.required),
            sport: new FormControl("", Validators.required),
            sportPraticeTime: new FormControl("", Validators.required),
            sportPraticeFrequency: new FormControl("", Validators.required)
        });
    }

    private patientDataFromFormValue(value: RegisterPatientFormValue) {
        const patient: Patient = Object.assign({}, value, {
            isDeleted: false,
            registrationTimestamp: null
        });

        return patient;
    } 

    //METODO ACIONADO AO CLICAR NO BOTÃO DE CADASTRAR
    cadastrarPaciente() {
        this.loadingProvider.displayPreloader("Cadastrando paciente");
        this.patient = this.patientDataFromFormValue(this.form.value);
        const currentUserUid: string = this.authProvider.currentUserUid;

        this.patientProvider.registerPacient(currentUserUid, this.patient)
            .then((res) => {
                const patientId: string = res.id;
                const exam: Fpi | Drop = this.examType === ExamType.FPI ? this.fpi : this.drop;
                let result: string;
                let footSide: string;
                let date: string;

                this.loadingProvider.setMessage("Vinculando teste ao paciente");
                switch (this.examType) {
                    case ExamType.FPI:
                        result = this.fpi.nm_resultado;
                        footSide = this.fpi.ic_pe;
                        date = this.fpi.dt_fpi;
                        break;
                    case ExamType.Drop:
                        result = this.drop.resultado;
                        footSide = this.drop.pe;
                        date = this.drop.dataDrop;
                        break;
                    default:
                        console.assert(this.examType !== null, "examType should not be null", this.examType);
                        console.assert(this.examType === ExamType.Drop || this.examType === ExamType.FPI, "The are currently only two avaible methods: FPI or Drop", this.examType);
                        break;
                }

                this.examProvider.addExam(currentUserUid, patientId, exam)
                    .then((response) => {
                        this.loadingProvider.hidePreloader();
                        const callback: () => void = () => {
                            if (this.examType === ExamType.FPI) {
                                this.recomendarOutro(footSide, patientId, this.patient.name);
                            }
                            else {
                                this.recomendarFpi();
                            }
                        };
                        this.mostrarResultado(result, footSide, date, callback);
                    })
                    .catch((error) => {
                        // TODO deal properly with an error while trying to attach a new exam to the recently created patient
                        console.error("error while trying to attach a new exam to the recently created patient: ", error);
                    });
            })
            .catch((error) => {
                // TODO deal with error when trying to register a new patient
                console.log("error while trying to register the patient: ", error);
            });
    }

    private mostrarResultado(resultado: string, pe: string, data: string, callback: () => void) {
        // MOSTRAR ALERT DIALOG COM O RESULTADO
        let alert = this.alertCtrl.create({
            title: 'Resultado',
            message: `<p class='no-margin'>O resultado do teste foi: ${resultado} </p>
                <p class='no-margin'>Paciente: ${this.patient.name}</p>
                <p class='no-margin'>Pé avaliado:  ${pe}</p>
                <p class='no-margin'>Tipo de teste:   ${this.examType}</p>
                <p class='no-margin'>Data/hora:  ${data} </p>`,
            buttons: [{
                text: 'Ok',
                role: 'cancel',
                handler: callback
            }]
        });
        alert.present();
    }

    private recomendarFpi() {
        let alert = this.alertCtrl.create({
            title: this.drop.resultado,
            message: "O teste mais completo e mais confiável é o F.P.I, recomendamos que o faça para cada paciente. Deseja realizar um teste F.P.I para o " + this.patient.name + " agora ? ",
            buttons: [{
                // PASSAR PARA PAGINA DE GERENCIAR PACIENTESt
                text: 'Não, Obrigado!',
                role: 'cancel',
                handler: () => {
                    this.navCtrl.push('GerenciarPacientesPage', {
                        userKey: this.navParams.get("userKey"),
                    })
                        .then(() => {
                            const startIndex = this.navCtrl.getActive().index - 3;
                            this.navCtrl.remove(startIndex, 3);
                        });
                }

            },
            {
                text: 'OK',
                handler: () => {
                    this.navCtrl.push('FpiPage', {
                        avaliar: "sim",
                        userKey: this.userKey,
                        pacienteKey: this.newPacienteKey,
                        pacienteNome: this.patient.name,
                        pe: this.drop.pe,
                        paginasARemover: 8
                    })
                }
            }]
        });
        alert.present();
    }

    private recomendarOutro(foot: string, patientId: string, patientName: string): void {
        const activePageIndex: number = this.navCtrl.getActive().index;
        const pagesToBeRemoved: number = 3;
        const startIndex: number = activePageIndex - pagesToBeRemoved + 1;

        const alert: Alert = this.alertCtrl.create({
            title: "Nossa sugestão",
            message: " É recomendável que faça-se um teste para cada pé. Deseja fazê-lo agora ?",
            buttons: [
                {
                    // PASSAR PARA PAGINA DE GERENCIAR PACIENTES
                    text: 'Não, Obrigado!',
                    role: 'cancel',
                    handler: () => {
                        this.navCtrl.push('TelaPacientePage', {
                            userKey: this.userKey,
                            patientId: patientId
                        })
                            .then(() => this.navCtrl.remove(startIndex, pagesToBeRemoved));
                    }
                },
                {
                    text: 'OK',
                    handler: () => {
                        // vendo qual pe foi avaliado para fazer o proximo teste com o outro pé
                        const oppositeFoot: string = foot === 'esquerdo' ? 'direito' : 'esquerdo';

                        this.navCtrl.push('FpiPage', {
                            avaliar: "sim",
                            userKey: this.userKey,
                            patientId: patientId,
                            patientName: patientName,
                            pe: oppositeFoot,
                        }).then(() => this.navCtrl.remove(startIndex, pagesToBeRemoved));
                    }
                }
            ]
        });

        alert.present();
    }

    onChangeHasLeftFootInjuries(hasInjuries: boolean): void {
        this.hasLeftFootInjuries = hasInjuries;
        const control: AbstractControl = this.form.get('leftFootInjuries');

        if (hasInjuries) {
            control.setValidators(Validators.required);
            control.enable();
        } else {
            control.clearValidators();
            control.disable();
        }

        this.form.updateValueAndValidity();
    }

    onChangeHasRightFootInjuries(hasInjuries: boolean): void {
        this.hasRightFootInjuries = hasInjuries;
        const control: AbstractControl = this.form.get('rightFootInjuries');
        
        if (hasInjuries) {
            control.setValidators(Validators.required);
            control.enable();
        } else {
            control.clearValidators();
            control.disable();
        }
        
        this.form.updateValueAndValidity();
    }

    onChangePraticeSport(doesPraticeSport: boolean): void {
        const sportPraticeTimeControl: AbstractControl = this.form.get("sportPraticeTime");
        const sportPraticeFrequencyControl: AbstractControl = this.form.get("sportPraticeFrequency");
        const sportControl: AbstractControl = this.form.get("sport");
        this.doesPraticeSport = doesPraticeSport;
        
        if (this.doesPraticeSport === true) {
            sportPraticeTimeControl.setValidators(Validators.required);
            sportPraticeFrequencyControl.setValidators(Validators.required);
            sportControl.enable();
        } else {
            sportPraticeTimeControl.clearValidators();
            sportPraticeFrequencyControl.clearValidators();

            this.patient.sport = null;
            sportControl.disable();
            sportPraticeTimeControl.setValue('');
            sportPraticeFrequencyControl.setValue('');
            
        }
        
        this.form.updateValueAndValidity();
    }
}
