import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DicionarioPage } from './dicionario';

@NgModule({
  declarations: [
    DicionarioPage,
  ],
  imports: [
    IonicPageModule.forChild(DicionarioPage),
  ],
})
export class DicionarioPageModule {}
