import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DropPePage } from './drop-pe';

@NgModule({
  declarations: [
    DropPePage,
  ],
  imports: [
    IonicPageModule.forChild(DropPePage),
  ],
  exports: [DropPePage]
})
export class DropPePageModule {}
