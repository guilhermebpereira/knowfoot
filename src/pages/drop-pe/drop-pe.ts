import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, Alert } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ExamType } from '../../models/enums/exam-type';
import { ExamProvider } from '../../providers/exam/exam';
import { AuthProvider } from '../../providers/auth/auth';
import { Drop, DropWithId } from '../../models/interfaces/drop';
import { LoadingProvider } from '../../providers/loading/loading';

@IonicPage()
@Component({
  selector: 'page-drop-pe',
  templateUrl: 'drop-pe.html',
})
export class DropPePage {

  userKey: string;
  patientId: string;
  private examId: string;

  drop: Drop | DropWithId;
  medidaSentada: number;
  medidaEmPe: number;
  form: FormGroup;
  pe: string;
  explicacao: boolean = false;
  iconName: string = "ios-arrow-down";
  private patientName: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController, 
    private examProvider: ExamProvider,
    private authProvider: AuthProvider,
    private loadingProvider: LoadingProvider
  ) {

    // armazenar valores
    this.userKey = this.navParams.get("userKey");
    this.medidaSentada = this.navParams.get("medidaSentada");
    this.medidaEmPe = this.navParams.get("medidaEmPeAnterior") || null;
    this.examId = this.navParams.get("examId") || null;
    this.patientId = this.navParams.get("patientId") || null;
    this.pe = this.navParams.get("pe");
    this.patientName = this.navParams.get("patientName");

    // instanciar form
    this.form = new FormGroup({
      medidaEmPe: new FormControl('', Validators.compose([Validators.min(1), Validators.max(50), Validators.required]))
    });
  }

  // mostrar / esconder o texto de explicação
  mostrarExplicacao(): void {
    if (this.explicacao) {
      this.explicacao = false;
      this.iconName = "ios-arrow-down"
    }
    else {
      this.explicacao = true;
      this.iconName = "ios-arrow-up"
    }
  }

  // MOSTRAR ALERT DIALOG COM O RESULTADO
  showResult(result: string, foot: string, formattedDate: string): void {
    const previousPageIndex: number = this.navCtrl.getActive().index - 1;

    const alert = this.alertCtrl.create({
      title: 'Resultado',
      message: `<p class='no-margin'>O resultado do teste foi:  ${result} </p>
                <p class='no-margin'>Paciente:  ${this.patientName} </p>
                <p class='no-margin'>Pé avaliado:  ${foot}</p>
                <p class='no-margin'>Tipo de teste:  DROP</p>
                <p class='no-margin'>Data/hora:  ${formattedDate}</p>`,
      buttons: [{
        role: 'cancel',
        text: 'Ok',
        handler: (value: any) => {
          this.popToPacientPage();
        },
      }]
    })
    alert.present();
  }

  private popToPacientPage(): void {
    const index = this.navCtrl.getActive().index - 2;

    this.navCtrl.remove(index, 2)
      .then(() => {
        this.navCtrl.pop();
      });
  }

  // método acionado ao clickar no botão "finalizar"
  finalizarDrop() {
    let valorResultado = this.medidaSentada - this.medidaEmPe;
    if (this.classificarResultado(valorResultado) != "invalido") 
    {
      const isNewPatient: boolean = this.navParams.get("avaliar") ? true : false;
      this.drop = isNewPatient ? {} as Drop : {} as DropWithId;
      this.drop.pe = this.pe;
      this.drop.medidaSentada = this.medidaSentada;
      this.drop.medidaEmPe = this.medidaEmPe;
      this.drop.valorResultado = valorResultado;
      this.drop.resultado = this.classificarResultado(valorResultado);
      this.drop.dataDrop = this.pegarDataAtual();
      this.drop.dataDrop2 = Date.now();

      if (!isNewPatient) {
        this.drop['id'] = this.examId;
      }

      if (this.navParams.get("avaliar") != null) {
        const userUid: string = this.authProvider.currentUserUid;
        this.addDropExam(userUid,this.patientId, this.drop)
      }
      else if(this.navParams.get("editar") != null)
      {
        const alert: Alert = this.createUpdateDropExamAlert(this.drop as DropWithId);
        alert.present();
      }
      else {
        this.navCtrl.push('CadastroPacientePage', {
          drop: this.drop,
          examType: ExamType.Drop,
        });
      }
    }
    else {
      this.alertCtrl.create({
        title: "Teste realizado incorretamente",
        message: "Favor consultar tutorial",
        buttons: ['OK']
      });
    }
  }

  private addDropExam(userUid: string,patientId: string, drop: Drop) {
    this.loadingProvider.displayPreloader("Gravando teste");

    this.examProvider.addExam(userUid, patientId, drop)
      .then(() => {
        this.loadingProvider.hidePreloader();
        this.showResult(drop.resultado, drop.pe, drop.dataDrop);
      })
      .catch((error) => {
        this.loadingProvider.hidePreloader();
        this.handleAddExamRequest(false);
      });
  }

  private createUpdateDropExamAlert(drop: DropWithId): Alert {
    const alert = this.alertCtrl.create({
      title: "Deseja realmente salvar as alterações do teste ?",
      subTitle: "Essa operação não pode ser desfeita.",
      buttons: [
        {
          text: "Sim, salvar",
          handler: () => {
            alert.dismiss()
              .then(() => {
                this.loadingProvider.displayPreloader("Editando teste");
                this.examProvider.updateExam(this.authProvider.currentUserUid, this.patientId, drop)
                  .then(() => {
                    this.loadingProvider.hidePreloader();
                    this.handleUpdateExamRequest(true, this.navParams.get("resultadoAnterior"), this.drop.resultado)
                  })
                  .catch((error) => {
                    this.loadingProvider.hidePreloader();
                    this.handleUpdateExamRequest(false);
                  });
              });
            return false;
          }
        },
        {
          text: "Não, obrigado",
          role: 'cancel'
        }
      ]
    });
    return alert;
  }

  private handleAddExamRequest(hadSuccessOnCreate: boolean): Promise<any> {
    const title: string = hadSuccessOnCreate ? "Sucesso" : "Erro";
    const message: string = hadSuccessOnCreate ? "Teste realizado com sucesso" : "Não foi possível concluir o teste";
    const alert: Alert = this.alertCtrl.create({
      "title": title,
      "message": message,
      buttons: [
        {
          text: "Ok",
          role: "cancel",
          handler: () => {
            const index = this.navCtrl.getActive().index - 1;
            this.navCtrl.remove(index, 2)
              .then(() => {
                this.navCtrl.pop();
              });
          }
        }
      ]
    });

    return alert.present();
  }

  private handleUpdateExamRequest(hadSuccessOnUpdate: boolean, previousResult?: string, newResult?: string): Promise<any> {
    const title: string = hadSuccessOnUpdate ? "Alterações salvas com sucesso!" : "Erro";
    const successMessage: string = `<h6 class='no-margin'>Resultado anterior: ${previousResult}</h6>
    <h6 class='no-margin'>Novo resultado:  ${newResult}</h6>`;
    const errorMessage: string = "Não foi possível atualizar o teste";

    const alert: Alert = this.alertCtrl.create({
      "title": title,
      message: hadSuccessOnUpdate ? successMessage : errorMessage,
      buttons: [
        {
          text: "OK",
          role: "cancel",
          handler: () => {
            const startIndex = this.navCtrl.getActive().index - 1;
            this.navCtrl.remove(startIndex)
              .then(() => {
                this.navCtrl.pop();
              });
          }
        }
      ]
    });

    return alert.present();
  }

  classificarResultado(valorResultado: number): string {
    if (valorResultado > 0) {
      if (valorResultado <= 1) {
        return "Altamente Supinado";
      }
      else if (valorResultado <= 4) {
        return "Supinado";
      }
      else if (valorResultado <= 9) {
        return "Normal";
      } else if (valorResultado <= 12) {
        return "Pronado";
      }
      else {
        return "Altamente Pronado";
      }
    }
    return "invalido";

  }
  // Pegar a data, horário e milissegundos
  private pegarDataAtual() {
    var date = new Date();
    var dia, mes, hora, minuto, segundo;
    var ndia = date.getDate();
    var nmes = date.getMonth() + 1;
    var ano = date.getFullYear();
    var nhora = date.getHours();
    var nminuto = date.getMinutes();
    var nsegundo = date.getSeconds();
    var milissegundo = date.getMilliseconds();
    if (ndia < 10) {
      dia = '0' + ndia;
    }
    else {
      dia = '' + ndia;
    }
    if (nmes < 10) {
      mes = '0' + nmes;
    }
    else {
      mes = '' + nmes;
    }
    if (nhora < 10) {
      hora = '0' + nhora;
    }
    else {
      hora = '' + nhora
    }
    if (nminuto < 10) {
      minuto = '0' + nminuto;
    }
    else {
      minuto = '' + nminuto;
    }
    if (nsegundo < 10) {
      segundo = '0' + nsegundo;
    }
    else {
      segundo = '' + nsegundo;
    }
    return dia + "/" + mes + "/" + ano + " " + hora + ":" + minuto;
  }
}
