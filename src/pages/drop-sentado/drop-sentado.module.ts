import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DropSentadoPage } from './drop-sentado';

@NgModule({
  declarations: [
    DropSentadoPage,
  ],
  imports: [
    IonicPageModule.forChild(DropSentadoPage),
  ],
  exports: [DropSentadoPage]
})
export class DropSentadoPageModule {}
