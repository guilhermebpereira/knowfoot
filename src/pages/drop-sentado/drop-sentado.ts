import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Validators, FormGroup, FormControl } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-drop-sentado',
  templateUrl: 'drop-sentado.html',
})
export class DropSentadoPage {

  medidaSentada: number;
  pe: String;
  explicacao: boolean = false;
  iconName: String = "ios-arrow-down";
  form: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.medidaSentada = this.navParams.get("medidaSentadaAnterior") || null;
    this.pe = this.navParams.get("pe");

    this.form = new FormGroup({
      medidaSentada: new FormControl({value: ''}, Validators.compose([Validators.min(1), Validators.max(50)]))
    });
  }

  mostrarExplicacao(): void
  {
    if(this.explicacao)
    {
      this.explicacao = false;
      this.iconName = "ios-arrow-down"
    }
    else
    {
      this.explicacao = true;
      this.iconName = "ios-arrow-up"
    }
  }

  proximaPosicao()
  {
    this.navCtrl.push('DropPePage', {
      userKey: this.navParams.get("userKey"),
      patientId: this.navParams.get("patientId") || null,
      examId: this.navParams.get("examId") || null,
      avaliar: this.navParams.get("avaliar") || null,
      medidaSentada: this.medidaSentada,
      resultadoAnterior: this.navParams.get("resultadoAnterior") || null,
      medidaEmPeAnterior: this.navParams.get("medidaEmPeAnterior"),
      editar: this.navParams.get("editar") || null,
      pe: this.pe,
      patientName: this.navParams.get("patientName")
    });
  }
}
