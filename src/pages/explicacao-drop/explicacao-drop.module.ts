import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExplicacaoDropPage } from './explicacao-drop';

@NgModule({
  declarations: [
    ExplicacaoDropPage,
  ],
  imports: [
    IonicPageModule.forChild(ExplicacaoDropPage),
  ],
  exports: [ExplicacaoDropPage]
})
export class ExplicacaoDropPageModule {}
