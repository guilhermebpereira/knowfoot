import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController, App, NavController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-explicacao-drop',
  templateUrl: 'explicacao-drop.html',
})
export class ExplicacaoDropPage {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public app: App,
    public viewCtrl: ViewController
  ) {}

  abrirDropPage() {
    this.app.getRootNavs()[0].push('DropSentadoPage', {
      userKey: this.navParams.get("userKey"),
      examId: this.navParams.get("examId") || null,
      patientId: this.navParams.get("patientId") || null,
      avaliar: this.navParams.get("avaliar") || null,
      pe: this.navParams.get("pe"),
      patientName: this.navParams.get("patientName")
    }).
      then(() => {
        const index = this.viewCtrl.index;
        this.navCtrl.remove(index)
      });
  }
}
