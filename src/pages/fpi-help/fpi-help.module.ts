import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FpiHelpPage } from './fpi-help';

@NgModule({
  declarations: [FpiHelpPage],
  imports: [IonicPageModule.forChild(FpiHelpPage)],
  exports: [FpiHelpPage]
})
export class FpiHelpPageModule {}
