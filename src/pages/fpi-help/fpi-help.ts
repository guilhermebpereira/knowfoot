import { ViewChild, Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Slides } from 'ionic-angular';
import { FpiHelpMessage } from '../../providers/fpi-help-message/fpi-help-message';

@IonicPage()
@Component({
  selector: 'page-fpi-help',
  templateUrl: 'fpi-help.html',
})
export class FpiHelpPage {
  @ViewChild(Slides) slides: Slides;
  
  // getting data from class static properties
  help: any;
  imageText: string[] = ["Altamente Supinado", "Supinado", "Neutro", "Pronado", "Altamente Pronado"];
  i: number = 0;
  // variables to params
  pagina: number;
  pe: string;
  constructor(public viewCtrl: ViewController, public navParams: NavParams) {
    this.pe = this.navParams.get("pe");
    this.pagina = this.navParams.get("pagina");
    if(this.pe === "esquerdo")
    {
      // const fpiHelpMessage: typeof FpiHelpMessage = FpiHelpMessage;
      this.help = FpiHelpMessage.esquerdo[this.pagina - 1]; 
    }
    else if(this.pe === "direito")
    {
        this.help = FpiHelpMessage.direito[this.pagina - 1]; 
    }
    else
    {
      console.error("O pé que esta sendo avalia do não é esquerdo nem direito");
      this.viewCtrl.dismiss();
    }
}

  ionViewDidEnter()
  {
    this.slides.slideTo(2);
    this.i = 2;
  }
  nextSlide() {
    this.slides.slideNext();
    this.i++;
  }
  
  previousSlide() {
    this.slides.slidePrev();
    this.i--;
  }


  closeModal()
  {
    this.viewCtrl.dismiss();
  }
}
