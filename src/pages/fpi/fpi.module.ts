import { NgModule} from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FpiPage } from './fpi';
@NgModule({
    declarations: [FpiPage],
    imports: [IonicPageModule.forChild(FpiPage)],
    exports: [FpiPage]
})

export class FpiPageModule{}