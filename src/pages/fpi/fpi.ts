import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Alert } from 'ionic-angular';
import { AlertController, ModalController } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { normalizeURL, ToastController } from 'ionic-angular';
import { Camera, CameraOptions} from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { Observar } from '../../classes/observar';
import { FpiImagens } from '../../classes/fpi-imagens';
import { ExamType } from '../../models/enums/exam-type';
import { Fpi, FpiWithId } from '../../models/interfaces/fpi';
import { ExamProvider } from '../../providers/exam/exam';
import { LoadingProvider } from '../../providers/loading/loading';
import { AuthProvider } from '../../providers/auth/auth';
import { MediaFile } from '../../models/interfaces/media-file';
import { FileProvider } from '../../providers/file/file';
import { FileType } from '../../models/enums/file-type';

declare var cordova: any;
declare let window: any;
@IonicPage()
@Component({
  selector: 'page-fpi',
  templateUrl: 'fpi.html',
})
export class FpiPage {
  textEscolha: string = "Escolha a imagem mais parecida";
  textObservar: string = "O que observar?";
  observar: boolean = false;
  pe: 'esquerdo' | 'direito';
  foto: boolean = false;
  fpi: Fpi;
  imageFilesURIs: string[] = [];
  public vl_parecida: number[] = [];
  public parecida: number[] = [];
  imgp = [];
  dataurl: string;
  readonly images: number[] = [0,1,2,3,4];
  readonly marcadores: number[] = [1,2,3,4,5];
  pagina: number;
  indexselecionada: number;
  private userKey: string;
  private patientId: string;
  private examId: string;
  private editar: string = null;
  private avaliar: string;
  private patientName: string;
  private frozenMedias: MediaFile[];

  constructor(  public navCtrl: NavController, public navParams: NavParams,
                private camera : Camera, private alertCtrl: AlertController,
                private platform: Platform, private file: File,
                private toastCtrl: ToastController, private fileProvider: FileProvider,
                public modalCtrl: ModalController, private examProvider: ExamProvider,
                private loadingProvider: LoadingProvider, private authProvider: AuthProvider
              ) {
    this.userKey = this.authProvider.currentUserUid;
    this.avaliar = null;
    this.pagina = this.navParams.get("pagina") > 1 ? this.navParams.get("pagina") : 1;
    this.patientName = this.navParams.get("patientName");
    this.imageFilesURIs = this.navParams.get("imageFilesURIs") || [];
    this.foto = this.imageFilesURIs[this.pagina - 1] ? true :   false;

      if (this.navParams.get("paginasARemover")) {
          const paginasARemover: number = this.navParams.get("paginasARemover");
          this.navCtrl.remove(this.navCtrl.getActive().index - 1, paginasARemover).then(() => {
              console.log(" nome: ", this.navCtrl.getByIndex(this.navCtrl.getActive().index - 1).name);
          });
      }

    if(this.navParams.get("avaliar") != null)
    {
      this.avaliar = this.navParams.get("avaliar");
      this.patientId = this.navParams.get("patientId");
    }
    else if(this.navParams.get("editar") != null) {
      this.patientId = this.navParams.get("patientId");
      this.editar = this.navParams.get("editar");
      this.examId = this.navParams.get("examId");
      this.frozenMedias = this.navParams.get("frozenMedias") || [];

      const hasImage: boolean = this.imageFilesURIs[this.pagina -1] ? true : false;
      if (hasImage) {
        this.attachImageURI(this.pagina, this.imageFilesURIs[this.pagina - 1]);
      }
    }
    
    // verificar se é pé esquerdo ou direito
    this.pe = this.navParams.get("pe");
    this.parecida = this.navParams.get("parecida") || [];
    this.vl_parecida = this.navParams.get("vl_parecida") || []; 

    if(this.parecida[this.pagina -1])
    {
      this.selecionarParecida(this.parecida[this.pagina -1]);
    }

   this.imgp = this.definirImagensFpi(this.pe);
}

  async deleteImage(fileURI: string): Promise<void> {
    this.fileProvider.deleteFile(fileURI);
    this.unattachImageURI(this.pagina);
  }

  resolveImageURL(fileURI: string): string {
    return window.Ionic.WebView.convertFileSrc(fileURI);
  }

    definirImagensFpi(pe: 'esquerdo' | 'direito'): string[] {
        let i: number = 0;
        let conjuntoFinal: string[] = [];

        if(pe === 'esquerdo') {
            FpiImagens.conjuntoImagensEsquerdo[this.pagina - 1].map(imagem => {
                conjuntoFinal[i] = imagem;
                i++;
            });
            
        } else {
                FpiImagens.conjuntoImagensDireito[this.pagina - 1].map(imagem => {
                    conjuntoFinal[i] = imagem;
                    i++;
                });
        }

        return conjuntoFinal;
    }

  clickouImagem(i: number)
  {
    if(this.observar)
    {
      this.oqueObservar(i);
    }
    else
    {
      this.selecionarParecida(i);
    }
  }

  oqueObservar(i: number)
  {
    this.alertCtrl.create({
      title: Observar.posicao[this.pagina - 1],
      message: Observar.textos[this.pagina -1].texto[i],
      buttons: ['OK']
    }).present();
  }

  toggleObservar()
  {
    this.observar = !this.observar;
    if(this.observar)
    {
      this.textObservar = "Já entendi";
      this.textEscolha = "Escolha a imagem que deseja entender";
    }
    else
    {
      this.textObservar = "O que observar?";
      this.textEscolha = "Escolha a imagem mais parecida";
    }
  }

  //----------- METODO QUANDO CLICA EM IMAGEM PARECIDA
  selecionarParecida(index: number): void {
    this.indexselecionada = index;
    this.parecida[this.pagina - 1] = index;

    switch(index) {
      case 0:
        this.vl_parecida[this.pagina - 1] = -2;
        break;
      case 1:
        this.vl_parecida[this.pagina - 1] = -1;
        break;
      case 2:
        this.vl_parecida[this.pagina - 1] = 0;
        break;
      case 3:
        this.vl_parecida[this.pagina - 1] = 1;
        break;
      case 4:
        this.vl_parecida[this.pagina - 1] = 2;
        break;
      default:  
        console.error("Numero index selecionado não está ent re 0 e 4");
        break;
    }
  }

  async takePictureAndAttach(): Promise<void> {
    let fileURI: string = await this.takePicture();
    const didCopied: boolean = await this.fileProvider.copyFile(fileURI, FileType.CopyImage);

    if (didCopied) {
      const fileName: string = this.fileProvider.parseFileName(fileURI);
      fileURI = this.fileProvider.resolveFileURI(FileType.CopyImage, fileName);
      this.attachImageURI(this.pagina, fileURI);
    } else {
      this.presentToast("Houve um erro ao processar a foto");
    }
  }

  private async takePicture(): Promise<string> {
    const cameraOptions: CameraOptions = {
      quality: 75,
      sourceType: this.camera.PictureSourceType.CAMERA,
      destinationType: this.camera.DestinationType.FILE_URI,
      cameraDirection: this.camera.Direction.BACK,
      saveToPhotoAlbum: false,
      mediaType: this.camera.MediaType.PICTURE,
      encodingType: this.camera.EncodingType.PNG,
      correctOrientation: true,
      targetWidth: 300,
      targetHeight: 400,
      allowEdit: true
    };

    return await this.camera.getPicture(cameraOptions)
      .catch(err => null);
  }
  // Pega a data, horário e milissegundos
  private pegarDataAtual(numero) {
    var date = new Date();
    var dia,mes,hora,minuto,segundo;
    var ndia = date.getDate();
    var nmes = date.getMonth() + 1;
    var ano = date.getFullYear();
    var nhora = date.getHours();
    var nminuto = date.getMinutes();
    var nsegundo = date.getSeconds();
    var milissegundo = date.getMilliseconds();
    if (ndia < 10) {
      dia = '0' + ndia;
    }
    else {
      dia = '' + ndia;
    }
    if (nmes < 10) {
      mes = '0' + nmes;
    }
    else {
      mes = '' + nmes;
    }
    if(nhora < 10)
    {
      hora = '0' + nhora;
    }
    else{
      hora = '' + nhora
    }
    if(nminuto < 10)
    {
      minuto = '0' + nminuto;
    }
    else{
      minuto = '' + nminuto;
    }
    if(nsegundo < 10)
    {
      segundo = '0' + nsegundo; 
    }
    else{
      segundo = '' + nsegundo;
    }
    // Exemplo 
    //fomato1: ddMMAAAAhhmmsS 
    if(numero === 2)
    {
       return  dia + mes + ano + hora + minuto + segundo + milissegundo;
    }
    return dia + "/" + mes + "/" + ano + " " + hora + ":" + minuto; 
  }
  
  // METODO DE MOSTRAR UMA TOASTMESSAGE
  private presentToast(message: string): Promise<any> {
    const toast = this.toastCtrl.create({
      message: message,
      duration: 6000,
      position: 'top'
    });

    return toast.present();
  }

  // MÉTODO DE SETAR NOME DO ARQUIVO DE IMAGEM NAS POSIÇÕES DO ARRAY.
  attachImageURI(pagina: number, fileURI: string): void {
      switch (pagina) {
        case 1:
          this.imageFilesURIs[0] = fileURI;
          this.imageFilesURIs[1] = fileURI;
          this.imageFilesURIs[2] = fileURI;
          break;
        case 2:
          this.imageFilesURIs[0] = fileURI;
          this.imageFilesURIs[1] = fileURI;
          this.imageFilesURIs[2] = fileURI;
          break;
        case 3:
          this.imageFilesURIs[0] = fileURI;
          this.imageFilesURIs[1] = fileURI;
          this.imageFilesURIs[2] = fileURI;
          break;
        case 4:
          this.imageFilesURIs[3] = fileURI;
          this.imageFilesURIs[4] = fileURI;
          break;
        case 5:
          this.imageFilesURIs[3] = fileURI;
          this.imageFilesURIs[4] = fileURI;
          break;
        default:
          console.log("Erro setando urls no array");
          break;
      }
      console.log("fileURI: ", fileURI);
      this.foto = true;
  }

  // MÉTODO DE DESSETAR NOME DO ARQUIVO DE IMAGEM NAS POSIÇÕES DO ARRAY.
  unattachImageURI(pagina: number): void {
    switch (pagina) {
      case 1:
        this.imageFilesURIs[0] = null;
        this.imageFilesURIs[1] = null;
        this.imageFilesURIs[2] = null;
        break;
      case 2:
        this.imageFilesURIs[0] = null;
        this.imageFilesURIs[1] = null;
        this.imageFilesURIs[2] = null;
        break;
      case 3:
        this.imageFilesURIs[0] = null;
        this.imageFilesURIs[1] = null;
        this.imageFilesURIs[2] = null;
        break;
      case 4:
        this.imageFilesURIs[3] = null;
        this.imageFilesURIs[4] = null;
        break;
      case 5:
        this.imageFilesURIs[3] = null;
        this.imageFilesURIs[4] = null;
        break;
      default:
        console.log("Erro DESsetando urls no array");
        break;
    } 
    this.foto = false;
  }
  
  abrirTutorialFpiPage()
  {
    this.navCtrl.push('TutorialFpiPage');
  }

  // METODO DE PASSAR PARA A PROXIMA PAGINA
  proxima()
  {
    if(this.indexselecionada >= 0 && this.indexselecionada <= 4)
    {
      this.pagina = this.pagina + 1;
      if(this.navParams.get("avaliar") != null)
      {
        this.navCtrl.push(FpiPage,{
          userKey: this.navParams.get('userKey'),
          patientId: this.patientId,
          pe: this.pe,
          pagina: this.pagina,
          parecida: this.parecida,
          vl_parecida: this.vl_parecida,
          imageFilesURIs: this.imageFilesURIs,
          patientName: this.navParams.get("patientName"),
          avaliar: this.avaliar,
          pagesToBeRemoved: this.navParams.get("pagesToBeRemoved")
        }).then(() => {
          // remover essa pagina da nav stack
          const startIndex = this.navCtrl.getActive().index - 1;
          this.navCtrl.remove(startIndex,1);
        });
        
      }
      
      else if(this.navParams.get("editar") != null)
      {
        this.navCtrl.push(FpiPage,{
          userKey: this.navParams.get('userKey'),
            patientId: this.patientId,
          pe: this.pe,
          pagina: this.pagina,
          parecida: this.parecida,
          vl_parecida: this.vl_parecida,
          editar: this.editar,
          examId: this.examId,
          imageFilesURIs: this.imageFilesURIs,
          frozenMedias: this.frozenMedias,
          patientName: this.navParams.get("patientName"),
          resultadoAnterior: this.navParams.get("resultadoAnterior")
        }).then(() => {
          // remover essa pagina da nav stack
          const startIndex = this.navCtrl.getActive().index - 1;
          this.navCtrl.remove(startIndex,1);
        }); 
      }
      else
      {
        this.navCtrl.push(FpiPage,{
          userKey: this.navParams.get('userKey'),
          patientId: this.patientId,
          pe: this.pe,
          pagina: this.pagina,
          parecida: this.parecida,
          vl_parecida: this.vl_parecida,
          imageFilesURIs: this.imageFilesURIs
        }).then(() => {
          // remover essa pagina da nav stack
          const startIndex = this.navCtrl.getActive().index - 1;
          this.navCtrl.remove(startIndex,1);
        });

      }
    }
    else
    {
      this.alertCtrl.create({
        title: "É necessário selecionar a imagem mais parecida.",
        buttons: ['OK']    
      }).present();
    }
  }
  
  // METODO DE PASSAR PARA A PAGINA ANTERIOR
  anterior()
  {
    this.pagina = this.pagina - 1;
    if(this.navParams.get("avaliar") != null)
    {
      this.navCtrl.push(FpiPage,{
        userKey: this.navParams.get("userKey"),
        patientId: this.navParams.get("patientId"),
        pe: this.pe,
        pagina: this.pagina,
        parecida: this.parecida,
        vl_parecida: this.vl_parecida,
        imageFilesURIs: this.imageFilesURIs,
        avaliar: this.avaliar,
        pagesToBeRemoved: this.navParams.get("pagesToBeRemoved")
      })
      .then(() => {
        // remover essa pagina da nav stack
        const startIndex = this.navCtrl.getActive().index - 1;
        this.navCtrl.remove(startIndex,1);
      });
      
    }
    else if(this.navParams.get("editar") != null)
    {
      this.navCtrl.push(FpiPage, {
        userKey: this.navParams.get('userKey'),
        patientId: this.patientId,
        pe: this.pe,
        pagina: this.pagina,
        parecida: this.parecida,
        vl_parecida: this.vl_parecida,
        imageFilesURIs: this.imageFilesURIs,
        editar: this.editar,
        examId: this.examId,
        frozenMedias: this.frozenMedias,
        patientName: this.navParams.get("patientName"),
        resultadoAnterior: this.navParams.get("resultadoAnterior")
      })
      .then(() => {
        // remover essa pagina da nav stack
        const startIndex = this.navCtrl.getActive().index - 1;
        this.navCtrl.remove(startIndex,1);
      });
    }
    else
    {
      this.navCtrl.push(FpiPage,{
        userKey: this.navParams.get("userKey"),
        patientId: this.patientId,
        pe: this.pe,
        pagina: this.pagina,
        parecida: this.parecida,
        vl_parecida: this.vl_parecida,
        imageFilesURIs: this.imageFilesURIs
      })
      .then(() => {
        // remover essa pagina da nav stack
        const startIndex = this.navCtrl.getActive().index - 1;
        this.navCtrl.remove(startIndex,1);
      });
    }
  }

  // METODO DE FINALIZAR SELEÇÃO
  finalizar() {
    if (this.indexselecionada >= 0 && this.indexselecionada <= 4) {
      const medias: MediaFile[] = [];

      this.imageFilesURIs.forEach((fileURI) => {
        if (fileURI) {
          const fileName: string = this.fileProvider.parseFileName(fileURI);
          const media: MediaFile = {
            fileName: fileName,
            fileType: FileType.CopyImage
          };

          medias.push(media);
        }
      });

      this.fpi = new Fpi();
      this.fpi.parecida = this.parecida;
      this.fpi.dt_fpi = this.pegarDataAtual(1);
      this.fpi.dt_fpi2 = Date.now();
      this.fpi.ic_pe = this.pe;
      this.fpi = this.calcularResultado(this.fpi);
      this.fpi.medias = medias;

      if (this.avaliar == null && this.editar == null) { // é um teste de um novo paciente
        this.navCtrl.push('CadastroPacientePage', {
          examType: ExamType.FPI,
          fpi: this.fpi
        });
      } else if (this.avaliar != null) { //  É uma novo teste de um paciente já existente
        const userUid: string = this.authProvider.currentUserUid;
        this.loadingProvider.displayPreloader("Concluíndo teste");

        this.examProvider.addExam(userUid, this.patientId, this.fpi)
          .then(() => {
            const patientName: string = this.navParams.get("patientName");
            const alert: Alert = this.createSuccessAddFPIAlert(this.fpi, patientName);

            this.loadingProvider.hidePreloader();
            alert.present();
          })
          .catch((error) => {
            const alert: Alert = this.createErrorUpdateFPIAlert();

            this.loadingProvider.hidePreloader();
            alert.present();
          });
      } else if (this.editar != null) {// É a edição de um teste já existente
        const examId: string = this.navParams.get("examId");
        const previousResult: string = this.navParams.get("resultadoAnterior");
        this.fpi['id'] = examId;
        const alert: Alert = this.createUpdateFPIExamAlert(this.fpi as FpiWithId, previousResult, this.fpi.nm_resultado, this.frozenMedias);
        alert.present();
      }
    }
    else {
      this.alertCtrl.create({
        title: "É necessário selecionar a imagem mais parecida.",
        buttons: ['OK']
      }).present();
    }
  }

  private createUpdateFPIExamAlert(fpi: FpiWithId, previousResult: string, newResult: string, previousMedias: MediaFile[] = []): Alert {
    const alert = this.alertCtrl.create({
      title: "Deseja realmente salvar as alterações do teste ?",
      subTitle: "Essa operação não pode ser desfeita",
      buttons: [
        {
          text: "Sim, salvar",
          handler: () => {
            alert.dismiss()
              .then(() => {
                const userUid: string = this.authProvider.currentUserUid;
                this.loadingProvider.displayPreloader("Gravando teste");

                this.examProvider.updateExam(userUid, this.patientId, fpi, previousMedias)
                  .then(() => {
                    const alert: Alert = this.createSuccessUpdateFPIAlert(previousResult, newResult);

                    this.loadingProvider.hidePreloader();
                    alert.present();
                  })
                  .catch((error) => {
                    const alert: Alert = this.createErrorUpdateFPIAlert();

                    this.loadingProvider.hidePreloader();
                    alert.present();
                  });
              });
            return false;
          }
        },
        {
          text: "Não, obrigado",
          role: 'cancel'
        }
      ]
    });
    
    return alert;
  }

  private createErrorUpdateFPIAlert(): Alert {
     const title: string = "Erro";
     const message: string = "Não foi possível atualizar o teste";
     const alert: Alert = this.alertCtrl.create({
      title: title,
      message: message,
      buttons: [
        {
          handler: () => {
            this.navCtrl.pop();
          },
          text: "Ok",
          role: "cancel"
        } 
      ]
    });

    return alert;
  }


  private createSuccessUpdateFPIAlert(previousResult: string, newResult: string): Alert {
    const title: string ="Alterações salvas com sucesso!";
    const message: string = `<h6 class='no-margin'>Resultado anterior: ${previousResult}</h6>
    <h6 class='no-margin'>Novo resultado: ${newResult}</h6>`;

      const alert: Alert = this.alertCtrl.create({
        "title": title,
        message: message,
        buttons: [
          {
            handler: () => {
              this.navCtrl.pop();
            },
            text: "Ok",
            role: "cancel"
          }
        ]  
      });
      return alert;
  }
   
  private createErrorAddFPIAlert(): Alert {
    const title: string = "Erro";
    const message: string = "Não foi possível concluir o teste";

    const alert: Alert = this.alertCtrl.create({
      title: title,
      message: message,
      buttons: [
        {
          text: "Ok",
          role: "cancel",
          handler: () => {
            const previousPageIndex: number = this.navCtrl.getActive().index;
            let pagesToBeRemoved: number = this.navParams.get("pagesToBeRemoved");
            pagesToBeRemoved = pagesToBeRemoved ? pagesToBeRemoved : 1;

            this.navCtrl.remove(previousPageIndex, pagesToBeRemoved)
              .then(() => {
                this.navCtrl.push("TelaPacientePage", {
                  userKey: this.userKey,
                  patientId: this.patientId
                });
              });
          }
        }
      ]
    });

    return alert;
  }

  private createSuccessAddFPIAlert(fpi: Fpi, patientName: string): Alert {
    const title: string = "Teste realizado com sucesso";
    const message: string = `
    <p class='no-margin'>O resultado do teste foi: ${fpi.nm_resultado} </p>
    <p class='no-margin'>Paciente:   ${patientName}</p> 
    <p class='no-margin'>Pé avaliado: ${fpi.ic_pe}</p> 
    <p class='no-margin'>Tipo de teste: ${ExamType.FPI}</p> 
    <p class='no-margin'>Data/hora: ${fpi.dt_fpi}</p>`;

    const alert: Alert = this.alertCtrl.create({
      title: title,
      message: message,
      buttons: [
        {
          text: "Ok",
          role: "cancel",
          handler: () => {

            this.navCtrl.push("TelaPacientePage", {
              userKey: this.userKey,
              patientId: this.patientId
            })
              .then(() => {
                const pagesToBeRemoved: number = this.navParams.get("pagesToBeRemoved") || 1;
                const startIndex: number = this.navCtrl.getActive().index - pagesToBeRemoved;
                

                this.navCtrl.remove(startIndex, pagesToBeRemoved);
              });
          }
        }
      ]
    });

    return alert;
  }

  calcularResultado(fpi: Fpi): Fpi
  {
    let i;
    let nresultado = 0;
    // RODANDO DA PAGINA 1 ATÉ A 5
    for (i = 0; i < 5; i++) {
      // somando o valor de cada escolha de usuário
      switch (this.vl_parecida[i]) {
        case -2:
          nresultado -= 2;
          break;
        case -1:
          nresultado -= 1;
          break;
        case 0:
          nresultado -= 0;
          break;
        case 1:
          nresultado += 1;
          break;
        case 2:
          nresultado += 2;
          break;
      }
      // passando o valor do vetor para as devidas "variáveis" do objeto 
          fpi.vl_parecida[i] = this.vl_parecida[i];
    }
    // Determinando o resultado de acordo com a pontuação
    if (nresultado >= -10 && nresultado <= -4) {
      fpi.nm_resultado = "Altamente Supinado";
    }
    else {
      if (nresultado >= -3 && nresultado <= -1) {
        fpi.nm_resultado = "Supinado";
      }
      else {
        if (nresultado >= 0 && nresultado <= 4) {
          fpi.nm_resultado = "Neutro";
        }
        else {
          if (nresultado >= 5 && nresultado <= 8) {
            fpi.nm_resultado = "Pronado";
          }
          else {
            fpi.nm_resultado = " Altamente Pronado";
          }
        }
      }
    }
    return fpi;
  }
}
