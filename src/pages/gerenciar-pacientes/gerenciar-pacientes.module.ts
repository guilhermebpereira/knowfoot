import { NgModule} from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GerenciarPacientesPage } from './gerenciar-pacientes';
import { PatientProvider } from '../../providers/patient/patient';

@NgModule({
    declarations: [GerenciarPacientesPage],
    imports: [IonicPageModule.forChild(GerenciarPacientesPage)],
    exports: [GerenciarPacientesPage],
    providers: [PatientProvider]
})

export class GerenciarPacientesPageModule{}