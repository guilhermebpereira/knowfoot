import { Component, OnDestroy } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Alert, LoadingController, Loading, ToastController, Toast } from 'ionic-angular';
import { Gesture } from 'ionic-angular';
import { Patient, PatientWithId } from '../../models/interfaces/patient';
import { AngularFirestoreCollection } from 'angularfire2/firestore';
import { PatientProvider } from '../../providers/patient/patient';
import { AuthProvider } from '../../providers/auth/auth';
import { DocumentChangeAction } from 'angularfire2/firestore/interfaces';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { LoadingProvider } from '../../providers/loading/loading';
import { Subject } from 'rxjs/Subject';
@IonicPage()
@Component({
  selector: 'page-gerenciar-pacientes',
  templateUrl: 'gerenciar-pacientes.html',
})

export class GerenciarPacientesPage implements OnDestroy {
  private pacienteKey: string;
  private onDestroy$: Subject<boolean> = new Subject<boolean>(); 
  patients: PatientWithId[];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private alertCtrl: AlertController,
    private patientProvider: PatientProvider,
    public authProvider: AuthProvider,
    private loadingProvider: LoadingProvider,
    private toastController: ToastController
  ) {
    if(this.navParams.get("editar") != null)
    {
      this.pacienteKey = this.navParams.get("pacienteKey");
      const startIndex = this.navCtrl.getActive().index - 1;
      this.navCtrl.remove(startIndex,2);
      this.openPatientDetailsPage(this.authProvider.currentUserUid, this.pacienteKey);
    }
  }

  ionViewDidLoad()
  {
    this.carregarPacientes();
  }

  ngOnDestroy() {
    this.onDestroy$.next(true);
    this.onDestroy$.unsubscribe();
  }

  carregarPacientes()
  {
    console.log("once");
    this.patientProvider.listPatients(this.authProvider.currentUserUid)
      .snapshotChanges()
      .map((patients) => {
        console.log("sdkaoskda: ", patients);
        return patients.map((patient) => {
          const id: string = patient.payload.doc.id;
          const data: Patient = patient.payload.doc.data() as Patient;
          const response: PatientWithId = { id, ...data };

          return response;
        })
      })
      .takeUntil(this.onDestroy$)
      .subscribe((patients) => {
        this.patients = patients;
      });
  }

  openPatientDetailsPage(userKey: string, patientId: string): Promise<any> {
    return this.navCtrl.push('TelaPacientePage', {
      "userKey": userKey, 
      "patientId": patientId
    });
  }

  private deletePatient(patientId: string, patientName: string): void {
    this.loadingProvider.displayPreloader('Excluindo paciente');
    
    const userUid: string = this.authProvider.currentUserUid;
    const toast: Toast = this.toastController.create({ duration: 5000 });

    this.patientProvider.deletePatient(userUid, patientId)
      .then(() => {
          toast.setMessage(`Paciente ${patientName} excluído com sucesso`);
          toast.present();
          this.loadingProvider.hidePreloader();
        })
        .catch((error) => {
          console.log("error: ", error);
          toast.setMessage("Não foi possível excluir o paciente");
          toast.present();
          this.loadingProvider.hidePreloader();
      });
  }


  confirmDeletePatient(patientId: string, patientName: string) {
    const alert: Alert = this.alertCtrl.create({
      title: "Tem certeza que deseja excluir esse paciente ?",
      buttons: [
        {
          text: "Cancelar",
          role: 'Cancel',
          handler: () =>{
            console.log("não quer excluir");
          }
        },  
        {
          text: "Sim",
          cssClass: "btn-warning",
          handler: () => {
              this.deletePatient(patientId, patientName);
          }
        }
    ]
    });
    
    alert.present();

  }
}
