import { Component } from '@angular/core';
import { IonicPage,NavController,NavParams, Platform } from 'ionic-angular';
import { PopoverController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { LoadingProvider } from '../../providers/loading/loading';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  userKey: string;
  constructor(
    public navCtrl: NavController, private navParams: NavParams, 
    public popoverCtrl: PopoverController, private statusBar: StatusBar,
    public loadingProvider: LoadingProvider, public platform: Platform) {
    if (this.navParams.get("login") != null) 
    {
      this.statusBar.show();
      this.userKey = this.navParams.get("userKey");
    }
  }
  mostrarSub(myEvent){
    let popover = this.popoverCtrl.create('PopoverPage');
    popover.present({
        ev:myEvent
    });
  }
  
  abrirMetodosTestesPage()
  {
    console.log("HOME: " + this.navParams.get("userKey"));
    this.navCtrl.push('MetodosTestesPage',{
      userKey: this.navParams.get("userKey")
    });
  }

  abrirMetodosTutoriaisPage()
  {
    this.navCtrl.push('MetodosTutoriaisPage');
  }

  // MÉTODO PARA CHAMAR A TELA DE GERENCIAR PACIENTES
  abrirGerenciarPacientesPage() {
    this.navCtrl.push('GerenciarPacientesPage', {
      userKey: this.navParams.get("userKey")
    });
  }

}
