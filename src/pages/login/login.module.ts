import { NgModule} from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginPage} from './login';
import { ToastProvider } from '../../providers/toast/toast';

@NgModule({
    declarations: [LoginPage],
    imports: [IonicPageModule.forChild(LoginPage)],
    exports: [LoginPage],
    providers: [ToastProvider]
})

export class LoginPageModule{}