import { Component } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { IonicPage, NavController, NavParams, Platform, Toast } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { ToastController } from 'ionic-angular';
import { LoadingProvider } from '../../providers/loading/loading';
import { ToastProvider } from '../../providers/toast/toast';
import { Constants } from '../../constants/constants';
import { AuthProvider } from '../../providers/auth/auth';

@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginPage {
    form: FormGroup;
    email: string = '';
    password: string = '';

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private statusBar: StatusBar,
        private formBuilder: FormBuilder,
        private toastCtrl: ToastController,
        private loadingProvider: LoadingProvider,
        public platform: Platform,
        private authProvider: AuthProvider,
        private toastProvider: ToastProvider
    ) {
        platform.ready().then(() => {
            this.statusBar.hide();
        });
        this.form = this.createForm();
    }

    private createForm(): FormGroup {
        return this.formBuilder.group({
            email: ['', Validators.required],
            password: ['', Validators.required]
        });
    }

    ionViewDidLoad() {
        this.statusBar.hide();
    }

    emailLogin(email: string, password: string): void {
        this.loadingProvider.displayPreloader("Aguarde...");

        this.authProvider.signInWithEmailAndPassword(email, password)
            .then((res) => {
                this.loadingProvider.hidePreloader();
                this.navCtrl.setRoot("HomePage");
            })
            .catch((error) => {
                this.loadingProvider.hidePreloader();
                this.handleLoginError(error);
            });
    }

    private handleLoginError(error: any): void {
        let message: string = "";

        if (error) {
            switch (error.code) {
                case Constants.auth.USER_NOT_FOUND:
                    message = "Usuário/senha inválidos";
                    break;
                case Constants.auth.WRONG_PASSWORD:
                    message = "Usuário/senha inválidos";
                    break;
                case Constants.auth.USER_DISABLED:
                    message = "O email utilizado foi desativado";
                    break;
                case Constants.auth.INVALID_EMAIL:
                    message = "Email inválido"
                    break;
                default:
                    message = "Não foi possível realizar o login";
                    break;
            }
        }

        const toast: Toast = this.toastProvider.createToast("login", message);
        toast.present();
    }

    abrirCadastrarUsuario() {
        this.navCtrl.push('CadastrarUsuarioPage');
    }
}
