import { NgModule} from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MetodosTestesPage } from './metodos-testes';

@NgModule({
    declarations: [MetodosTestesPage],
    imports: [IonicPageModule.forChild(MetodosTestesPage)],
    exports: [MetodosTestesPage]
})

export class MetodosTestesPageModule{}