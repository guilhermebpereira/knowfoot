import { Component } from '@angular/core';
import { ViewController, IonicPage, NavController, NavParams, ModalController, Modal } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-metodos-testes',
  templateUrl: 'metodos-testes.html',
})
export class MetodosTestesPage {
  private cd_paciente: any;
  private userKey: string;
  private patientId: string;
  private avaliar: string;
  private patientName: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private modalCtrl: ModalController
  ) {
    this.avaliar = null;
    this.userKey = this.navParams.get("userKey");
    this.patientName = this.navParams.get("patientName");

    if(this.navParams.get("avaliar") != null)
    {
      this.patientId = this.navParams.get("patientId");
      this.avaliar = this.navParams.get("avaliar");
      this.cd_paciente = this.navParams.get("cd_paciente");
    }
  }

  abrirMetodoUm(pe): Promise<any> {
  const pagesToBeRemoved: number = this.avaliar ? 3 : null;

    return this.navCtrl.push('FpiPage', {
      userKey: this.userKey,
      patientId: this.patientId || null,
      pe: pe,
      avaliar: this.avaliar || null,
      patientName: this.patientName,
      'pagesToBeRemoved': pagesToBeRemoved
    });
  }

  abrirMetodoDois(pe: 'direito' | 'esquerdo'): Promise<any> {
    const modal: Modal = this.modalCtrl.create('ExplicacaoDropPage', {
      userKey: this.userKey,
      patientId: this.patientId || null,
      pe: pe,
      avaliar: this.avaliar || null,
      patientName: this.patientName
    });

    return modal.present();
  }
}
