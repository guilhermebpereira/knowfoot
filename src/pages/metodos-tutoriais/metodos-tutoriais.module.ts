import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MetodosTutoriaisPage } from './metodos-tutoriais';

@NgModule({
  declarations: [
    MetodosTutoriaisPage,
  ],
  imports: [
    IonicPageModule.forChild(MetodosTutoriaisPage),
  ],
})
export class MetodosTutoriaisPageModule {}
