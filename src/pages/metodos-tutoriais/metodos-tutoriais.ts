import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the MetodosTutoriaisPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-metodos-tutoriais',
  templateUrl: 'metodos-tutoriais.html',
})
export class MetodosTutoriaisPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  abrirTutorialFpiPage()
  {
    this.navCtrl.push("TutorialFpiPage")
  }

  abrirTutorialDropPage()
  {
    this.navCtrl.push("TutorialDropPage")
  }
}
