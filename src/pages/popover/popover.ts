import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-popover',
  templateUrl: 'popover.html'
})
export class PopoverPage {
  constructor(private navCtrl: NavController, public viewCtrl: ViewController) { }

  abrirDicionario() {
    this.navCtrl.push('DicionarioPage');
  }

  sair() {
    this.viewCtrl.dismiss()
      .then(() => {
        this.navCtrl.setRoot('LoginPage')
      })
  }
}