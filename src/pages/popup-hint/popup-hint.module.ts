import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PopupHintPage } from './popup-hint';

@NgModule({
  declarations: [PopupHintPage],
  imports: [IonicPageModule.forChild(PopupHintPage)],
  exports: [PopupHintPage]
})
export class PopupHintPageModule {}
