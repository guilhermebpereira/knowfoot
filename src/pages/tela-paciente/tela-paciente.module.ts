import { NgModule} from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TelaPacientePage} from './tela-paciente';
import { PatientProvider } from "../../providers/patient/patient";
@NgModule({
    declarations: [TelaPacientePage],
    imports: [IonicPageModule.forChild(TelaPacientePage)],
    exports: [TelaPacientePage],
    providers: [PatientProvider]
})

export class TelaPacientePageModule{}