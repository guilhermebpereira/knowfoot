import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Alert } from 'ionic-angular';
import { Validators, FormGroup, AbstractControl, FormBuilder, ValidationErrors } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { PatientProvider } from '../../providers/patient/patient';
import { StorageReference } from 'firebase/storage';
import { File } from '@ionic-native/file';
import firebase from 'firebase'; 
import { Patient, PatientWithId } from '../../models/interfaces/patient';
import { HelperProvider } from '../../providers/helper/helper';
import { Gender } from '../../models/enums/gender';
import { Sport, SportPraticeTime, SportPraticeFrequency } from '../../models/enums/sports';
import { Injurie } from '../../models/enums/injurie';
import { FootSide } from '../../models/enums/foot-side';
import { KeyValuePair } from '../../models/interfaces/key-value-pair';
import { take } from 'rxjs/operators/take';
import { AuthProvider } from '../../providers/auth/auth';
import { LoadingProvider } from '../../providers/loading/loading';
import { Drop, DropWithId } from '../../models/interfaces/drop';
import { FpiWithId, Fpi } from '../../models/interfaces/fpi';
import { AngularFirestoreDocument } from "angularfire2/firestore";
import { FileProvider } from "../../providers/file/file";
import { MediaFile } from "../../models/interfaces/media-file";
import { FileType } from "../../models/enums/file-type";

declare var cordova: any;

@IonicPage()
@Component({
  selector: 'page-tela-paciente',
  templateUrl: 'tela-paciente.html',
  
})

export class TelaPacientePage {
  userKey: string;
  patientId: string;
  editarTestes: boolean = false;
  exams: Observable<(FpiWithId | DropWithId)[]>;
  form: FormGroup;
  model: Patient;
  model2: Patient;
  editarTexto: string;
  perfilTexto: string;
  editarPaciente: boolean = false;
  isPatientDataExpanded: boolean = false;
  editarTestesText: string = "Editar";
  sports: KeyValuePair[];
  injuries: KeyValuePair[];
  footSides: KeyValuePair[];
  sportPraticeFrequencyOptions: KeyValuePair[];
  genderOptions: KeyValuePair[];
  sportPraticeTimeOptions: KeyValuePair[];
  footSideEnum = FootSide;
  notHasLeftFootInjuries: boolean = false;
  notHasRightFootInjuries: boolean = false;
  doesNotPraticeSport: boolean = false;
  expandedExamCardIndex: number = -1;

  private ref: any = firebase.database().ref(); 
  private photosRef: StorageReference = firebase.storage().ref('/Photos/');
  private pacienteObsersable$: AngularFirestoreDocument<Patient>;

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private patientProvider: PatientProvider,
    private alertCtrl: AlertController,
    private file: File,
    private helperProvider: HelperProvider,
    private authProvider: AuthProvider,
    private loadingProvider: LoadingProvider,
    private formBuilder: FormBuilder,
    private fileProvider: FileProvider
  ) 
    {
      this.patientId = this.navParams.get("patientId");
      this.userKey = this.navParams.get("userKey");
      
      this.sports = this.helperProvider.translateEnum(Sport);
      this.injuries = this.helperProvider.translateEnum(Injurie);
      this.footSides = this.helperProvider.translateEnum(FootSide);
      this.sportPraticeTimeOptions = this.helperProvider.translateEnum(SportPraticeTime);
      this.sportPraticeFrequencyOptions = this.helperProvider.translateEnum(SportPraticeFrequency);
      this.genderOptions = this.helperProvider.translateEnum(Gender);

      this.form = this.createForm();
      this.form.disable({ onlySelf: false });

      if (this.userKey != null && this.patientId != null) {
        this.editarTexto = "Editar";
        this.perfilTexto = "Mostrar mais"
      } else {
        console.assert(this.userKey != null, "The TelaPacientePage requires a userKey parameter to be passed as data", this.userKey);
        console.assert(this.patientId != null, "The TelaPacientePage requires a patientId parameter to be passed as data", this.patientId);
      }
      
  }

  private createForm(): FormGroup {
    const requiredAndEmpty: (string | ((control: AbstractControl) => ValidationErrors))[] = ['', Validators.required];

    const form: FormGroup = this.formBuilder.group({
      name: ['', Validators.compose([Validators.required, Validators.maxLength(55)])],
      height: ['', Validators.compose([Validators.required, Validators.min(50), Validators.max(300)])],
      birthYear: ['', Validators.compose([Validators.required, Validators.min(1910), Validators.max(2018)])],
      notHasLeftFootInjuries: requiredAndEmpty,
      notHasRightFootInjuries: requiredAndEmpty,
      weight: requiredAndEmpty,
      gender: requiredAndEmpty,
      dominantFoot: requiredAndEmpty,
      leftFootInjuries: requiredAndEmpty,
      rightFootInjuries: requiredAndEmpty,
      doesNotPraticeSport: requiredAndEmpty,
      sport: requiredAndEmpty,
      sportPraticeTime: requiredAndEmpty,
      sportPraticeFrequency: requiredAndEmpty
    });

    return form;
  }

  ionViewDidLoad() { 
    const userUid: string = this.authProvider.currentUserUid;
    this.loadPatientDetails();
    this.loadPatientExams(userUid, this.patientId);
  }

  filterExamImages(mediaFiles: MediaFile[]): MediaFile[] {
    const filteredMedias: MediaFile[] = [];

    if (mediaFiles) {
      for (const media of mediaFiles) {
        const alreadyAdded: boolean = filteredMedias.some((filteredMedia) => filteredMedia.downloadURL === media.downloadURL);
  
        if (!alreadyAdded) {
          filteredMedias.push(media);
        }
      }
    }

    return filteredMedias;
  }

  // método de clonar um objeto( shallow copy)
  clone(obj) 
  {
    return Object.assign({}, obj);
  }

  private loadPatientDetails(): void {
    this.pacienteObsersable$ = this.patientProvider.patient(this.userKey, this.patientId);
    this.pacienteObsersable$.valueChanges()
      .pipe(take(1))
      .subscribe(res => {
        this.model = this.clone(res);
        this.model2 = this.clone(res);
        this.notHasLeftFootInjuries = this.model2.leftFootInjuries ? false : true;
        this.notHasRightFootInjuries = this.model2.rightFootInjuries ? false : true;
        this.doesNotPraticeSport = this.model2.sport ? false : true;

        const leftFootInjuriesControl: AbstractControl = this.form.get("leftFootInjuries");
        const rightFootInjuriesControl: AbstractControl = this.form.get("rightFootInjuries");
        const sportControl: AbstractControl = this.form.get("sport");
        const sportPraticeTimeControl: AbstractControl = this.form.get("sportPraticeTime");
        const sportPraticeFrequencyControl: AbstractControl = this.form.get("sportPraticeFrequency");
        

        this.updateControlValidation(!this.notHasLeftFootInjuries, leftFootInjuriesControl);
        this.updateControlValidation(!this.notHasRightFootInjuries, rightFootInjuriesControl);
        this.updateControlValidation(!this.doesNotPraticeSport, sportControl);
        this.updateControlValidation(!this.doesNotPraticeSport, sportPraticeTimeControl);
        this.updateControlValidation(!this.doesNotPraticeSport, sportPraticeFrequencyControl);
        
        this.form.updateValueAndValidity();
    });
  }
  
  loadPatientExams(userUid: string, patientId: string): void {
    this.exams = this.patientProvider.listPatientExams(userUid, patientId)
    .snapshotChanges()
    .map((exams) => {
      return exams.map((response) => {
        const id: string = response.payload.doc.id;
        const examData: Fpi | Drop = response.payload.doc.data() as Fpi | Drop;
        const exam: FpiWithId | DropWithId = { id, ...examData };

        return exam;
      });
    });
  }
  
  // fazer novo teste do paciente
  avaliar(patientName: string, userKey: string, patientId: string): Promise<any> {
    return this.navCtrl.push('MetodosTestesPage', {
      avaliar: "sim",
      "userKey": userKey,
      "patientId": patientId,
      "patientName": patientName
    });
  }
  
  // salvar alterações nos dados do paciente
  salvar()
  {
    let alert = this.alertCtrl.create({
      title: "Deseja realmente salvar as alterações ?",
      buttons: [
        {
          text: "Cancelar",
          role: 'Cancel',
        },
        {
          text: "Sim",
          handler: () => {
            alert.dismiss().then(() => {
              this.loadingProvider.displayPreloader("Atualizando dados");
              const alert: Alert = this.alertCtrl.create({
                buttons: [
                  {
                    role: "cancel",
                    text: "Ok"
                  }
                ]
              });

              const patient: Patient = { ...this.model};
              
              patient.leftFootInjuries = this.notHasLeftFootInjuries ? "" : patient.leftFootInjuries;
              patient.rightFootInjuries = this.notHasRightFootInjuries ? "" : patient.rightFootInjuries;
              patient.sport = this.doesNotPraticeSport ? null : patient.sport;
              patient.sportPraticeTime = this.doesNotPraticeSport ? null : patient.sportPraticeTime;
              patient.sportPraticeFrequency = this.doesNotPraticeSport ? null : patient.sportPraticeFrequency;

               this.patientProvider.updatePatientData(this.authProvider.currentUserUid, this.patientId, patient)
                .then(() => {
                  this.loadingProvider.hidePreloader();
                  alert.setTitle("Sucesso");
                  alert.setMessage("Dados atualizados com sucesso");
                  alert.present();
                  this.model2 = { ...patient};
                  this.ativarEditarPaciente();
                })
                .catch((error) => {
                  alert.setTitle("Erro");
                  this.loadingProvider.hidePreloader()
                  alert.setMessage("Não foi possível atualizar os dados");
                  alert.present();
                });
            });
            return false;
          }
        }
    ]
    });
    alert.present();
  }

  toggleIsPatientDataExpanded(): void {
    const showLessText: string = "Mostrar menos";
    const showMoreText: string = "Mostrar mais";
    
    this.isPatientDataExpanded = !this.isPatientDataExpanded;
    this.perfilTexto = this.isPatientDataExpanded ? showLessText : showMoreText;
  }

  //método de editar teste clickado
  editarTeste(exam: FpiWithId | DropWithId): void
  {
    if(this.editarTestes)
    {
      const isFPI: boolean = exam['dt_fpi'] ? true : false;
      const promise: Promise<any> = isFPI ? this.editarTesteFpi(exam as FpiWithId) : this.editarTesteDrop(exam as DropWithId);
      
      promise.then(() => this.ativarEditarTestes());
    }
  }

  private editarTesteDrop(exam: DropWithId): Promise<any> {
    return this.navCtrl.push('DropSentadoPage', {
      examId: exam.id,
      userKey: this.userKey,
      patientId: this.patientId,
      pe: exam.pe,
      editar: "sim",
      resultadoAnterior: exam.resultado,
      medidaSentadaAnterior: exam.medidaSentada,
      medidaEmPeAnterior: exam.medidaEmPe
    });
  }

  private async editarTesteFpi(fpi: FpiWithId): Promise<any> {
    const frozenMedias: MediaFile[] = (JSON.parse(JSON.stringify(fpi)) as Fpi).medias;
    fpi.medias = await this.copyMediaFiles(fpi.medias);
    const imageFilesURIs: string[] = fpi.medias.map((media) => this.fileProvider.resolveFileURI(media.fileType, media.fileName));


    return this.navCtrl.push('FpiPage', {
      examId: fpi.id,
      userKey: this.userKey,
      patientId: this.patientId,
      parecida: fpi.parecida,
      vl_parecida: fpi.vl_parecida,
      pe: fpi.ic_pe,
      frozenMedias: frozenMedias,
      resultadoAnterior: fpi.nm_resultado,
      'imageFilesURIs': imageFilesURIs,
      editar: "sim"
    });
  }

  private async copyMediaFiles(mediaFiles: MediaFile[]): Promise<MediaFile[]> {
    const medias: MediaFile[] = [];

    for (const mediaFile of mediaFiles) {
      const alreadyCopied: boolean = medias.some((media) => media.fileName === mediaFile.fileName);
      if (!alreadyCopied) {
        const fileURI: string = this.fileProvider.resolveFileURI(mediaFile.fileType, mediaFile.fileName);
        
        await this.fileProvider.copyFile(fileURI, FileType.CopyImage);
        mediaFile.fileType = FileType.CopyImage;
      }

      medias.push(mediaFile);
    }

    return medias;
  }

  updateExpandedExamCard(cardIndex: number): void {
    this.expandedExamCardIndex = this.expandedExamCardIndex === cardIndex ? -1 : cardIndex;
  }

  // METODO DE TRANSFORMAR O ARQUIVO SALVO EM BASE64 URL
  transformarDataUrl(imageName): Promise<string>
  {
    console.log("cordova.file.dataDirectory: " + cordova.file.dataDirectory);
    console.log("imagename: " + imageName);
    return this.file.readAsDataURL(cordova.file.dataDirectory, imageName)
    .then(dataurl => {
      return dataurl;
    },
  (error) =>{
    return "erro";
  });
  }
  
  ativarEditarTestes()
  {
    if(this.editarTestes)
    {
      this.editarTestes = false;
      this.editarTestesText = "Editar";
    }
    else
    {
      this.editarTestes = true;
      this.editarTestesText = "Cancelar";
    }
  }

  ativarEditarPaciente(): void {
    const leftFootInjuries: Injurie[] | string = this.model2.leftFootInjuries;
    const rightFootInjuries: Injurie[] | string = this.model2.rightFootInjuries;
    const hasLeftFootInjuries: boolean = leftFootInjuries != null && leftFootInjuries != "";
    const hasRightFootInjuries: boolean = rightFootInjuries != null && rightFootInjuries != "";
    const sport: Sport | string = this.model2.sport;
    const doesPraticeSport: boolean = sport != null && sport != "";
    const leftFootInjuriesControl: AbstractControl = this.form.get('leftFootInjuries');
    const rightFootInjuriesControl: AbstractControl = this.form.get('rightFootInjuries');
    const sportControl: AbstractControl = this.form.get("sport");
    const sportPraticeTimeControl: AbstractControl = this.form.get("sportPraticeTime");
    const sportPraticeFrequencyControl: AbstractControl = this.form.get("sportPraticeFrequency");

    if(this.editarPaciente) {
      this.editarPaciente = false;
      this.editarTexto = "Editar";
      this.isPatientDataExpanded = false;
      this.perfilTexto = "Mostrar mais";
      this.model = this.model2;
      this.model2 = this.clone(this.model2);
      
      this.form.disable({ onlySelf: false });
      // this.model is the copy number 1 and this.model2 is the 'unchanged' copy number 2 
    }
    else {
      this.editarPaciente = true; 
      this.editarTexto = "Cancelar";
      this.isPatientDataExpanded = true;
      this.perfilTexto = "Mostrar menos";
      
      this.form.enable({ onlySelf: false });
      this.updateControlEnabledStatus(hasRightFootInjuries, rightFootInjuriesControl);
      this.updateControlEnabledStatus(hasLeftFootInjuries, leftFootInjuriesControl);
      this.updateControlEnabledStatus(doesPraticeSport, sportControl);
      this.updateControlEnabledStatus(doesPraticeSport, sportPraticeTimeControl);
      this.updateControlEnabledStatus(doesPraticeSport, sportPraticeFrequencyControl);
    }
    
    this.form.updateValueAndValidity();
  }

  onChangeHasLeftFootInjuries(notHasInjuries: boolean): void {
    const controlName: string = 'leftFootInjuries'
    const control: AbstractControl = this.form.get(controlName);

    this.updateControlValidation(!notHasInjuries, control);
    this.updateControlEnabledStatus(!notHasInjuries, control);
    this.form.updateValueAndValidity();
  }

  onChangePraticeSport(doesNotPraticeSport: boolean) {
    const sportControl: AbstractControl = this.form.get("sport");
    const sportPraticeTimeControl: AbstractControl = this.form.get("sportPraticeTime");
    const sportPraticeFrequencyControl: AbstractControl = this.form.get("sportPraticeFrequency");

    this.updateControlValidation(!doesNotPraticeSport, sportControl)
    this.updateControlValidation(!doesNotPraticeSport, sportPraticeTimeControl),
    this.updateControlValidation(!doesNotPraticeSport, sportPraticeFrequencyControl);

    this.updateControlEnabledStatus(!doesNotPraticeSport, sportControl)
    this.updateControlEnabledStatus(!doesNotPraticeSport, sportPraticeTimeControl),
    this.updateControlEnabledStatus(!doesNotPraticeSport, sportPraticeFrequencyControl);
    
    this.form.updateValueAndValidity();
  }

  onChangeHasRightFootInjuries(notHasInjuries: boolean): void {
    const controlName: string = 'rightFootInjuries';
    const control: AbstractControl = this.form.get(controlName);
    
    this.updateControlValidation(!notHasInjuries, control);
    this.updateControlEnabledStatus(!notHasInjuries, control);
    this.form.updateValueAndValidity();
  }

  private updateControlValidation(isValueNeeed: boolean, control: AbstractControl): void {
    if (isValueNeeed) {
      control.setValidators(Validators.required);
    } else {
      control.clearValidators();
    }
  }

  private updateControlEnabledStatus(isValueNeeed: boolean, control: AbstractControl): void {
    if (isValueNeeed) {
      control.enable({ onlySelf: true });
    } else {
      control.disable({ onlySelf: true });
    }
  }
}