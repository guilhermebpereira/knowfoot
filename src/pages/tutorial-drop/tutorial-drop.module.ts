import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TutorialDropPage } from './tutorial-drop';

@NgModule({
  declarations: [
    TutorialDropPage,
  ],
  imports: [
    IonicPageModule.forChild(TutorialDropPage),
  ],
})
export class TutorialDropPageModule {}
