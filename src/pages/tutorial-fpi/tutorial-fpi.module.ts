import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TutorialFpiPage } from './tutorial-fpi';

@NgModule({
  declarations: [
    TutorialFpiPage,
  ],
  imports: [
    IonicPageModule.forChild(TutorialFpiPage),
  ],
})
export class TutorialFpiPageModule {}
