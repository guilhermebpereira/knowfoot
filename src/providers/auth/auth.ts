import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { User } from '../../models/interfaces/user';
import { Storage } from '@ionic/storage';

@Injectable()
export class AuthProvider {
  private uid:   string

  constructor(
    private fireAuth: AngularFireAuth,
    private firestore: AngularFirestore,
    private storage: Storage
  ) {
    this.storage.get('uid').then((uid) => {
      this.uid = uid;
    });
  }

  signInWithEmailAndPassword(email: string, password: string): Promise<any> {
    let promise: Promise<any>;

    promise = new Promise((resolve, reject) => {
      this.fireAuth.auth.signInWithEmailAndPassword(email, password)
        .then((value) => {
          const uid = this.fireAuth.auth.currentUser.uid;
          const callback: () => void = () => {
            this.uid = uid;
            resolve(value)
          };
          const errorCallback: (error) => void = (error)=> {
            reject(error);
          };
          
          this.storeUserUid(uid, callback, errorCallback);
        })
        .catch((error) => {
          reject(error);
        });
    });

    return promise;
  }

  private storeUserUid(uid: string, successCallback: () => void, errorCallback: (error) => void): void {
    this.storage.set('uid', uid)
      .then(() => successCallback())
      .catch((error) => errorCallback(error));
  }


  createUserWithEmailAndPassword(email: string, password: string): Promise<string> {
    let promise: Promise<string>

    promise = new Promise<string>((resolve, reject) => {
      this.fireAuth.auth.createUserWithEmailAndPassword(email, password)
        .then((response) => {
          const uid = this.fireAuth.auth.currentUser.uid;
          const callback: () => void = () => {
            this.uid = uid;
            resolve(uid)
          };
          const errorCallback: (error) => void = (error) => {
            reject(error);
          };

          this.storeUserUid(uid, callback, errorCallback);
        })
        .catch((error) => reject(error))
    });

    return promise;
  }

  get currentUserUid(): string {
    return this.uid != null ? '' + this.uid : null;
  }
}
