import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import 'rxjs/add/operator/map';
import { ToastController } from 'ionic-angular';

@Injectable()
export class DatabaseProvider {

  // Código retirado do endereço http://www.fabricadecodigo.com/crud-sqlite-ionic/, no dia 15/12/2017, às 15:04
  constructor(private sqlite: SQLite, private toastCtrl: ToastController) {}

  public getDB()
  {
    return this.sqlite.create({
      name: 'banco.db',
      location: 'default'
    });
  }

  public createDatabase()
  {
    return this.getDB()
    .then((db: SQLiteObject) => {
      // CRIAR TABELAS
      this.createTables(db);
      this.insertDefaultItems(db);
    })
    .catch(e => console.error("this.getDB(): " + e.message));
  }

  public createTables(db: SQLiteObject)
  {
    db.sqlBatch([
      ['CREATE TABLE IF NOT EXISTS tb_usuario'+
      ' (cd_usuario VARCHAR(45) primary key NOT NULL,'+
      ' nm_usuario VARCHAR(55) NOT NULL, vl_ano INTEGER NOT NULL,'+
      ' nm_profissao VARCHAR(45) NOT NULL, nm_escolaridade VARCHAR(25) NOT NULL,'+
      ' nm_login_usuario VARCHAR(35) NOT NULL, nm_senha VARCHAR(25) NOT NULL)'],

      ['CREATE TABLE IF NOT EXISTS tb_paciente'+
      ' (cd_paciente integer primary key AUTOINCREMENT NOT NULL,'+
      ' nm_paciente VARCHAR(45) NOT NULL, vl_altura INTEGER NOT NULL,'+
      ' vl_idade INTEGER NOT NULL, vl_peso INTEGER NOT NULL,'+ 
      ' ic_sexo CHAR(1) NOT NULL, ic_lesao CHAR(1), cd_lesao INTEGER,'+
      ' ic_pratica CHAR(1) NOT NULL, nm_esporte VARCHAR(45),'+
      ' nm_tempo VARCHAR(35), nm_frequencia VARCHAR(35), cd_usuario VARCHAR(45),'+
      ' FOREIGN KEY(cd_usuario) REFERENCES tb_usuario(cd_usuario))'],
      
      ['CREATE TABLE IF NOT EXISTS tb_lesao '+
      '(cd_lesao INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,'+
      'cd_lesao1 VARCHAR(35) NOT NULL, cd_lesao2 VARCHAR(35),'+
      'cd_lesao3 VARCHAR(35), cd_lesao4 VARCHAR(35), cd_lesao5 VARCHAR(35))'],                                             
                         
      ['CREATE TABLE IF NOT EXISTS tb_fpi '+
      '(cd_fpi INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,' +
      ' vl_parecida1 INTEGER NOT NULL, vl_parecida2 INTEGER NOT NULL,'+
      ' vl_parecida3 INTEGER NOT NULL, vl_parecida4 INTEGER NOT NULL,'+
      ' vl_parecida5 INTEGER NOT NULL, vl_parecida6 INTEGER NOT NULL,'+
      ' nm_image1 VARCHAR(100), nm_image2 VARCHAR(100), nm_image3 VARCHAR(100),'+
      ' dt_fpi VARCHAR(20) NOT NULL, nm_resultado VARCHAR(25),'+
      ' ic_pe INTEGER NOT NULL, cd_paciente INTEGER NOT NULL,'+
      ' FOREIGN KEY(cd_paciente) REFERENCES tb_paciente(cd_paciente))']
    ])
    .catch(e => console.error('Erro ao criar tabelas: ' + e.message));
  }

  private insertDefaultItems(db: SQLiteObject)
  {
    db.executeSql('SELECT COUNT(cd_lesao) FROM tb_lesao', {})
    .then((data: any) => {
      // verificar se a tb_lesão já não foi populada
      if(data.rows.item(0).cd_leso == 0)
      {
        // populando tb_lesao
        db.sqlBatch([
          /* Entorse de tornozelo - 1
          Fascite Plantar - 2
          Esporão de Calcâneo - 3
          Metatarsalgia - 4
          Outro(s) - 5*/
          ['INSERT INTO tb_lesao  (cd_lesao1) VALUES (?)', [1]],
          ['INSERT INTO tb_lesao (cd_lesao1) VALUES (?)', [2]],
          ['INSERT INTO tb_lesao (cd_lesao1) VALUES (?)', [3]],
          ['INSERT INTO tb_lesao (cd_lesao1) VALUES (?)', [4]],
          ['INSERT INTO tb_lesao (cd_lesao1) VALUES (?)', [5]],
          ['INSERT INTO tb_lesao (cd_lesao1, cd_lesao2) VALUES (?,?)', [1,2]],
          ['INSERT INTO tb_lesao (cd_lesao1, cd_lesao2) VALUES (?,?)', [1,3]],
          ['INSERT INTO tb_lesao (cd_lesao1, cd_lesao2) VALUES (?,?)', [1,4]],
          ['INSERT INTO tb_lesao (cd_lesao1, cd_lesao2) VALUES (?,?)', [1,5]],
          ['INSERT INTO tb_lesao (cd_lesao1, cd_lesao2) VALUES (?,?)', [2,3]],
          ['INSERT INTO tb_lesao (cd_lesao1, cd_lesao2) VALUES (?,?)', [2,4]],
          ['INSERT INTO tb_lesao (cd_lesao1, cd_lesao2) VALUES (?,?)', [2,5]],
          ['INSERT INTO tb_lesao (cd_lesao1, cd_lesao2) VALUES (?,?)', [3,4]],
          ['INSERT INTO tb_lesao (cd_lesao1, cd_lesao2) VALUES (?,?)', [3,5]],
          ['INSERT INTO tb_lesao (cd_lesao1, cd_lesao2) VALUES (?,?)', [4,3]],
          ['INSERT INTO tb_lesao (cd_lesao1, cd_lesao2,cd_lesao3) VALUES (?,?,?)', [1,2,3]],
          ['INSERT INTO tb_lesao (cd_lesao1, cd_lesao2,cd_lesao3) VALUES (?,?,?)', [1,2,4]],
          ['INSERT INTO tb_lesao (cd_lesao1, cd_lesao2,cd_lesao3) VALUES (?,?,?)', [1,2,5]],
          ['INSERT INTO tb_lesao (cd_lesao1, cd_lesao2,cd_lesao3) VALUES (?,?,?)', [1,3,4]],
          ['INSERT INTO tb_lesao (cd_lesao1, cd_lesao2,cd_lesao3) VALUES (?,?,?)', [1,3,5]],
          ['INSERT INTO tb_lesao (cd_lesao1, cd_lesao2,cd_lesao3) VALUES (?,?,?)', [1,4,5]],
          ['INSERT INTO tb_lesao (cd_lesao1, cd_lesao2,cd_lesao3) VALUES (?,?,?)', [2,3,4]],
          ['INSERT INTO tb_lesao (cd_lesao1, cd_lesao2,cd_lesao3) VALUES (?,?,?)', [2,3,5]],
          ['INSERT INTO tb_lesao (cd_lesao1, cd_lesao2,cd_lesao3) VALUES (?,?,?)', [2,4,5]],
          ['INSERT INTO tb_lesao (cd_lesao1, cd_lesao2,cd_lesao3) VALUES (?,?,?)', [3,4,5]],
          ['INSERT INTO tb_lesao (cd_lesao1, cd_lesao2,cd_lesao3,cd_lesao4) VALUES (?,?,?,?)', [1,2,3,4]],
          ['INSERT INTO tb_lesao (cd_lesao1, cd_lesao2,cd_lesao3,cd_lesao4) VALUES (?,?,?,?)', [1,2,3,5]],
          ['INSERT INTO tb_lesao (cd_lesao1, cd_lesao2,cd_lesao3,cd_lesao4) VALUES (?,?,?,?)', [1,2,4,5]],
          ['INSERT INTO tb_lesao (cd_lesao1, cd_lesao2,cd_lesao3,cd_lesao4) VALUES (?,?,?,?)', [1,3,4,5]],
          ['INSERT INTO tb_lesao (cd_lesao1, cd_lesao2,cd_lesao3,cd_lesao4) VALUES (?,?,?,?)', [2,3,4,5]],
          ['INSERT INTO tb_lesao (cd_lesao1, cd_lesao2,cd_lesao3,cd_lesao4,cd_lesao5) VALUES (?,?,?,?,?)', [1,2,3,4,5]]
        ])
        .then(() => this.presentToast("tb_lesão populada com sucesso!"))
        .catch((e) => this.presentToast("Erro ao popular tb_lesao: " + e));
      }
    })
  }
  private presentToast(text) 
  {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 2000,
      position: 'top'
    });
    toast.present();
  }
}
