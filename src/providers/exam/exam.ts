import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import firebase from 'firebase';
import { Drop, DropWithId } from '../../models/interfaces/drop';
import { Fpi, FpiWithId } from '../../models/interfaces/fpi';
import { MediaProvider } from '../media/media';
import { FileProvider } from '../file/file';
import { MediaFile } from '../../models/interfaces/media-file';
import { FileType } from '../../models/enums/file-type';

@Injectable()
export class ExamProvider {
  constructor(
    private fileProvider: FileProvider,
    private firestore: AngularFirestore,
    private mediaProvider: MediaProvider
  ) { }

  async addExam(userUid: string, patientId: string, exam: Fpi | Drop): Promise<firebase.firestore.DocumentReference> {
    const isFpi: boolean = exam['dt_fpi'] ? true : false;

    if (isFpi) {
      const fpiExam: Fpi = { ...exam } as Fpi;
      const filteredMedias: MediaFile[] = [];

      for (const media of fpiExam.medias) {
        const alreadyAdded: boolean = filteredMedias.some((filteredMedia) => filteredMedia.fileName === media.fileName);

        if (!alreadyAdded) {
          filteredMedias.push(media);
        }
      }

      for (const filteredMedia of filteredMedias) {
        const uploadedMedia = await this.mediaProvider.uploadMediaFile(filteredMedia);

        fpiExam.medias = fpiExam.medias.map((media) => {
          if (media.fileName === filteredMedia.fileName) {
              return uploadedMedia;
          } else {
            return media;
          }
        });
      }

      (exam as Fpi).medias = fpiExam.medias;
    }

    exam = JSON.parse(JSON.stringify(exam));
    return this.firestore.collection<Fpi | Drop>(`users/${userUid}/patients/${patientId}/exams`).add(exam);
  }

  async updateExam(userUid: string, patientId: string, exam: FpiWithId | DropWithId, previousMedias: MediaFile[] = []): Promise<void> {
    const isFpi: boolean = exam['dt_fpi'] ? true: false;
    const date: Date = new Date();
    // TODO stop using local date and start using server date insteads
    const formattedDate: string = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
    const formattedHour: string = date.getHours() + ':' + date.getMinutes();
    const formattedDateAndHour: string = formattedDate + ' ' + formattedHour;
    const nonDuplicatedMediaFiles: MediaFile[] = [];

    if (isFpi) {
      const fpiExam: Fpi = exam as Fpi;
      const checkedMediaFiles: MediaFile[] = [];

      for (const media of fpiExam.medias) {
        const alreadyAdded: boolean = nonDuplicatedMediaFiles.some((mediaFile) => mediaFile.fileName === media.fileName);

        if (!alreadyAdded) {
          nonDuplicatedMediaFiles.push(media);
        }
      }

      for (const mediaFile of nonDuplicatedMediaFiles) {
        const uploadedMedia = await this.mediaProvider.uploadMediaFile(mediaFile);

        fpiExam.medias = fpiExam.medias.map((media) => {
          if (media.fileName === mediaFile.fileName) {
              return uploadedMedia;
          } else {
            return media;
          }
        });
      }


      previousMedias.forEach(async (previousMedia) => {
        const alreadyChecked: boolean = checkedMediaFiles.some((mediaFile) => mediaFile.fileName === previousMedia.fileName);
        if (!alreadyChecked) {
          checkedMediaFiles.push(previousMedia);
          const wasOverrided: boolean = !fpiExam.medias.some((fpiMedia) => fpiMedia.fileName === previousMedia.fileName);
          
  
          if (wasOverrided) {
            const fileURI: string = this.fileProvider.resolveFileURI(previousMedia.fileType, previousMedia.fileName);
            this.mediaProvider.deleteMediaFile(previousMedia);
            this.fileProvider.deleteFile(fileURI);
          }
        }
      });
      return this.firestore.doc(`users/${userUid}/patients/${patientId}/exams/${exam.id}`).update({
          parecida: fpiExam.parecida,
          vl_parecida: fpiExam.vl_parecida,
          nm_image: fpiExam.nm_image,
          dt_fpi: formattedDateAndHour,
          dt_fpi2: date.getTime(),
          nm_resultado: fpiExam.nm_resultado,
          ic_pe: fpiExam.ic_pe,
          medias: fpiExam.medias
      } as Fpi);
    } else {
      const dropExam: Drop = exam as Drop;

      return this.firestore.doc(`users/${userUid}/patients/${patientId}/exams/${exam.id}`).update({
        pe: dropExam.pe,
        medidaSentada: dropExam.medidaSentada,
        medidaEmPe: dropExam.medidaEmPe,
        valorResultado: dropExam.valorResultado,
        resultado: dropExam.resultado,
        dataDrop: formattedDateAndHour,
        dataDrop2: date.getTime()
      });
    }
  }
}
