import { File, RemoveResult } from "@ionic-native/file";
import { Injectable } from "@angular/core";
import { FileType } from "../../models/enums/file-type";
import { Platform } from "ionic-angular";

@Injectable()
export class FileProvider {
  private readonly fileTypeEnum = FileType;
  private readonly baseFolder: string = this.file.dataDirectory;
  private readonly foldersNames = {
    image: this.fileTypeEnum.Image.toLowerCase(),
    copyImage: this.fileTypeEnum.CopyImage.toLowerCase()
  };

  constructor(
    private file: File,
    private platform: Platform,
  ) {
    if (this.platform.is("cordova")) {
      this.createDefaultFolders(this.foldersNames);
    }
  }

  private async createDefaultFolders(foldersNames: any): Promise<void> {
    const keys: string[] = Object.keys(foldersNames);

    await this.platform.ready();

    keys.forEach(async (key) => {
      const alreadyExists: boolean = await this.file.checkDir(this.baseFolder, foldersNames[key])
        .catch(() => false);

      if (!alreadyExists) {
        await this.file.createDir(this.baseFolder, foldersNames[key], false);
      };
    });

  }

  async checkFile(fileURI: string, fileType: FileType): Promise<boolean> {
    const path: string = this.resolvePath(fileType);
    const fileName: string = this.parseFileName(fileURI);

    try {
      const response: boolean = await this.file.checkFile(path, fileName);
      
      return response;
    } catch(error) {
      return false;
    }
  }

  copyFile(fileURI: string, fileType: FileType): Promise<boolean> {
    const fileName: string = this.parseFileName(fileURI);
    const filePath: string = this.parseFilePath(fileURI);
    const newPath: string = this.resolvePath(fileType);
  
    return new Promise<boolean>((resolve) => {
      this.file.copyFile(filePath, fileName, newPath, fileName)
        .then(() => resolve(true))
        .catch((error) => resolve(false));
    });
  }

  async convertToBlob(fileURI: string, type: 'image' = 'image'): Promise<Blob> {
    const path: string = this.parseFilePath(fileURI);
    const fileName: string = this.parseFileName(fileURI);
    const splittedFileName: string[] = fileName.split('.');
    const fileExtension: string = splittedFileName[splittedFileName.length - 1];
    const blobType = `${type}/${fileExtension}`;

    try {
      const arrayBuffer: ArrayBuffer = await this.file.readAsArrayBuffer(path, fileName);
      const blob: Blob = new Blob([arrayBuffer], { type: blobType });

      return blob;
    } catch(error) {
      return null;
    }
  }

  parseFileName(fileURI: string): string {
    const splittedFileURI: string[] = fileURI.split('/');
    const fileName: string = splittedFileURI.pop();

    return fileName;
  }

 resolveFileURI(fileType: FileType, fileName: string): string {
     const path: string = this.resolvePath(fileType);
     const fullPath: string = path + fileName;

     return fullPath;
 }

  parseFilePath(fileURI: string): string {
    const splittedFileURI: string[] = fileURI.split('/');
    const fileNameIndex: number = splittedFileURI.length - 1;
    const splittedPath: string[] = splittedFileURI.slice(0, fileNameIndex);
    const path: string = `${splittedPath.join('/')}/`;

    return path;
  }

  resolvePath(fileType: FileType): string {
    const folderName: string = this.resolveFolderName(fileType);

    return `${this.baseFolder}${folderName}/`;
  }

  deleteFile(fileURI: string): Promise<boolean> {
    const path: string = this.parseFilePath(fileURI);
    const fileName: string = this.parseFileName(fileURI);

    return new Promise<boolean>((resolve) => {
      this.file.removeFile(path, fileName)
        .then((result: RemoveResult) => resolve(result.success))
        .catch((error) => resolve(false));
    });
  }

  resolveFolderName(fileType: FileType): string {
    let folderName: string;

    switch (fileType) {
      case FileType.Image:
        folderName = this.foldersNames.image;
        break;
      case FileType.CopyImage:
        folderName = this.foldersNames.copyImage;
        break;
      default:
        throw new Error('The value to fileType parameter does not match acceptables values. See FileType enum');
    }

    return folderName;
  }
}