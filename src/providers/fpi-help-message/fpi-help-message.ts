export class FpiHelpMessage{
    static esquerdo = [
        {
            nome: "1- Curvaturas acima e abaixo do maléolo lateral",
            texto: "Nessa avaliação, deve-se vizualizar o centro do calcanhar em um ângulo sem curvas para com o mesmo. O item avaliado são as curvas(conforme destacado em vermelho)",
            imgSrcs: ["../../assets/imgs/1-esquerdo-menos2.png", "../../assets/imgs/1-esquerdo-menos1.png", "../../assets/imgs/1-esquerdo-zero.png", "../../assets/imgs/1-esquerdo-mais1.png", "../../assets/imgs/1-esquerdo-mais2.png"]
        },
        {
            nome: "2- Posição do calcâneo",
            texto: "Nessa avaliação, deve-se vizualizar o centro do calcanhar em um ângulo sem curvas para com o mesmo",
            imgSrcs: ["../../assets/imgs/2-esquerdo-menos2.png", "../../assets/imgs/2-esquerdo-menos1.png", "../../assets/imgs/2-esquerdo-zero.png", "../../assets/imgs/2-esquerdo-mais1.png", "../../assets/imgs/2-esquerdo-mais2.png"]
        },
        {
            nome: "3 - Abdução e Adução do antepé sobre o retropé",
            texto: "Nessa avaliação, deve-se vizualizar o centro do calcanhar em um ângulo sem curvas para com o mesmo",
            imgSrcs: ["../../assets/imgs/3-esquerdo-menos2.png", "../../assets/imgs/3-esquerdo-menos1.png", "../../assets/imgs/3-esquerdo-zero.png", "../../assets/imgs/3-esquerdo-mais1.png", "../../assets/imgs/3-esquerdo-mais2.png"]
        },
        {
            nome: "4- Altura do arco longitudinal medial",
            texto: "Nessa avaliação, deve-se vizualizar o centro do calcanhar em um ângulo sem curvas para com o mesmo",
            imgSrcs: ["../../assets/imgs/4-esquerdo-menos2.png", "../../assets/imgs/4-esquerdo-menos1.png", "../../assets/imgs/4-esquerdo-zero.png", "../../assets/imgs/4-esquerdo-mais1.png", "../../assets/imgs/4-esquerdo-mais2.png"]
        },
        {
            nome: "5- Proeminência na região da articulação talonavicular (ATN)",
            texto: "Nessa avaliação, deve-se vizualizar o centro do calcanhar em um ângulo sem curvas para com o mesmo",
            imgSrcs: ["../../assets/imgs/4-esquerdo-menos2.png", "../../assets/imgs/4-esquerdo-menos1.png", "../../assets/imgs/4-esquerdo-zero.png", "../../assets/imgs/4-esquerdo-mais1.png", "../../assets/imgs/4-esquerdo-mais2.png"]
        },
    ];

    static direito = [
        {
            nome: "1- Curvaturas acima e abaixo do maléolo lateral",
            texto: "Nessa avaliação, deve-se vizualizar o centro do calcanhar em um ângulo sem curvas para com o mesmo",
            imgSrcs: ["../../assets/imgs/1-direito-menos2.png", "../../assets/imgs/1-direito-menos1.png", "../../assets/imgs/1-direito-zero.png", "../../assets/imgs/1-direito-mais1.png", "../../assets/imgs/1-direito-mais2.png"]
        },
        {
            nome: "2- Posição do calcâneo",
            texto: "Nessa avaliação, deve-se vizualizar o centro do calcanhar em um ângulo sem curvas para com o mesmo",
            imgSrcs: ["../../assets/imgs/2-direito-menos2.png", "../../assets/imgs/1-direito-menos1.png", "../../assets/imgs/1-direito-zero.png", "../../assets/imgs/1-direito-mais1.png", "../../assets/imgs/1-direito-mais2.png"]
        },
        {
            nome: "3 - Abdução e Adução do antepé sobre o retropé",
            texto: "Nessa avaliação, deve-se vizualizar o centro do calcanhar em um ângulo sem curvas para com o mesmo",
            imgSrcs: ["../../assets/imgs/3-direito-menos2.png", "../../assets/imgs/3-direito-menos1.png", "../../assets/imgs/3-direito-zero.png", "../../assets/imgs/3-direito-mais1.png", "../../assets/imgs/3-direito-mais2.png"]
        },
        {
            nome: "4- Altura do arco longitudinal medial",
            texto: "Nessa avaliação, deve-se vizualizar o centro do calcanhar em um ângulo sem curvas para com o mesmo",
            imgSrcs: ["../../assets/imgs/4-direito-menos2.png", "../../assets/imgs/4-direito-menos1.png", "../../assets/imgs/4-direito-zero.png", "../../assets/imgs/4-direito-mais1.png", "../../assets/imgs/4-direito-mais2.png"]
        },
        {
            nome: "5- Proeminência na região da articulação talonavicular (ATN)",
            texto: "Nessa avaliação, deve-se vizualizar o centro do calcanhar em um ângulo sem curvas para com o mesmo",
            imgSrcs: ["../../assets/imgs/4-direito-menos2.png", "../../assets/imgs/4-direito-menos1.png", "../../assets/imgs/4-direito-zero.png", "../../assets/imgs/4-direito-mais1.png", "../../assets/imgs/4-direito-mais2.png"]
        },
    ];
}
