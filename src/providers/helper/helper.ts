import { Injectable } from "@angular/core";
import { KeyValuePair } from "../../models/interfaces/key-value-pair";
import { NavController, ViewController } from "ionic-angular";

@Injectable()
export class HelperProvider {
  translateEnum(enumType: any): KeyValuePair[] {
    const items: KeyValuePair[] = [];
    const keys: string[] = Object.keys(enumType);
    
    keys.map((key) => {
      items.push({
        key: key,
        value: enumType[key]
      });
    });
    
    return items;
  }
  
  removePreviousPage(navController: NavController): Promise<any> {
    const previousPageIndex: number = navController.getActive().index -1 ;
    
    return navController.remove(previousPageIndex, 1);
  }
}
