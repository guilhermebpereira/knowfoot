import { Injectable } from '@angular/core';
import { Loading, LoadingController } from 'ionic-angular';

/*
  Generated class for the Preloader provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class LoadingProvider {

   private loading: Loading;

   constructor( public loadingCtrl : LoadingController){}

   displayPreloader(mensagem: string) : void
   {
      this.loading = this.loadingCtrl.create({
         spinner: 'crescent',
         content: mensagem
      });
      this.loading.present();
   }
   

   setMessage(mensagem: string) : void
   {
     this.loading.setContent(mensagem);
   }



   hidePreloader() : void
   {
      this.loading.dismiss();
   }
}