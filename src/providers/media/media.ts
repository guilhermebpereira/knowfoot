import { Injectable } from '@angular/core';
import { AngularFireStorage, AngularFireStorageReference } from 'angularfire2/storage';
import { FileType } from '../../models/enums/file-type';
import { FileProvider } from '../file/file';
import { File } from '@ionic-native/file';
import { MediaFile } from '../../models/interfaces/media-file';
@Injectable()
export class MediaProvider {
  constructor(
    private file: File,
    private fileProvider: FileProvider,
    private storage: AngularFireStorage
  ) { }

  uploadMediaFile(mediaFile: MediaFile): Promise<MediaFile> {
    const file: MediaFile = { ...mediaFile }; 
    const mediaFileType: FileType = file.fileType;
    const mediaFileName: string = file.fileName;

    const folderName: string = this.fileProvider.resolveFolderName(FileType.Image);
    const fileURI: string = this.fileProvider.resolveFileURI(mediaFileType, mediaFileName);
    const fileName: string = this.fileProvider.parseFileName(fileURI);

    const reference: AngularFireStorageReference = this.storage.ref(`${folderName}/${fileName}`);

    return new Promise<MediaFile>(async (resolve) => {
      try {
        await this.fileProvider.copyFile(fileURI, FileType.Image);
        file.fileName = fileName;
        file.fileType = FileType.Image;
        const blob: Blob = await this.fileProvider.convertToBlob(fileURI);

        if (blob == null) {
          resolve(file);
        }

        await reference.put(blob).then();

        reference.getDownloadURL().subscribe(
          (downloadURL: string) => {
            file.downloadURL = downloadURL;
            resolve(file);
          },
          (error) => resolve(file)
        );

      } catch (error) {
        resolve(file);
      }
    });
  }

  deleteMediaFile(mediaFile: MediaFile): Promise<boolean> {
    const folderName: string = this.fileProvider.resolveFolderName(mediaFile.fileType);
    const reference: AngularFireStorageReference = this.storage.ref(`${folderName}/${mediaFile.fileName}`);

    return new Promise<boolean>((resolve) => {
      reference.delete().take(1).subscribe(
          (response) => resolve(true),
          (error) => resolve(false)
        );
    })
  }
} 