import { Injectable } from '@angular/core';
import { Patient } from '../../models/interfaces/patient';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import firebase from 'firebase';
import { OrderBy } from '../../models/enums/order-by';
import { Fpi } from '../../models/interfaces/fpi';
import { Drop } from '../../models/interfaces/drop';

@Injectable()
export class PatientProvider {

    constructor(private firestore: AngularFirestore) { }

    patient(userUid: string, patientKey: string): AngularFirestoreDocument<Patient> {
        return this.firestore.doc(`users/${userUid}/patients/${patientKey}`);
    }

    registerPacient(userUid, patient: Patient): Promise<firebase.firestore.DocumentReference> {
        patient.registrationTimestamp = firebase.firestore.FieldValue.serverTimestamp();

        return this.firestore.collection(`users/${userUid}/patients`)
            .add(patient);
    }

    listPatients(userUid: string, orderBy: OrderBy = OrderBy.Ascending): AngularFirestoreCollection<Patient> {
        return this.firestore.collection(`users/${userUid}/patients`, ref => ref.where("isDeleted", '==', false)
            .orderBy('registrationTimestamp', orderBy));
    }

    deletePatient(userKey: string, patientId: string): Promise<void> {
        return this.firestore.doc(`users/${userKey}/patients/${patientId}`)
            .update({ isDeleted: true });
    }

    listPatientExams(userUid: string, patientId: string): AngularFirestoreCollection<Fpi | Drop> {
        // TODO add order by option
        return this.firestore.collection(`users/${userUid}/patients/${patientId}/exams`);
    }

    updatePatientData(userUid: string, patientId: string, patient: Patient): Promise<void> {
        return this.firestore.doc(`users/${userUid}/patients/${patientId}`).update({
            name: patient.name,
            height: patient.height,
            birthYear: patient.birthYear,
            weight: patient.weight,
            gender: patient.gender,
            dominantFoot: patient.dominantFoot,
            sport: patient.sport,
            leftFootInjuries: patient.leftFootInjuries,
            rightFootInjuries: patient.rightFootInjuries,
            sportPraticeTime: patient.sportPraticeTime,
            sportPraticeFrequency: patient.sportPraticeFrequency
        });
    }
}
