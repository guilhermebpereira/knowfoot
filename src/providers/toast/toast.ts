import { Injectable } from '@angular/core';
import { Toast, ToastController } from 'ionic-angular';

@Injectable()
export class ToastProvider {

  private toasts = {};

  constructor(private toastController: ToastController) {}

  createToast(id: string, message: string = "", position: string = "top", duration: number = 3000): Toast {
    const toast: Toast = this.toastController.create({
      "message": message,
      "position": position,
      "duration": duration,
    });
    return toast;
  }
}
