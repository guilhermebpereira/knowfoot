import { ValidatorFn, FormControl, ValidationErrors } from "@angular/forms";

export class CustomValidators {
    static mirror(otherControlName: string, message): ValidatorFn {
        let control: FormControl;

        return (receveidControl: FormControl): ValidationErrors | null => {
            if (!receveidControl) {
                return null;
            }

            if (!receveidControl.value) {
                return null;
            }
            
            if (receveidControl.parent) {

                if (!control) {
                    control = receveidControl;
                    control.parent.get(otherControlName).statusChanges.subscribe(() => {
                        control.updateValueAndValidity();
                    });
                }

                const otherControlValue: any = receveidControl.parent.get(otherControlName).value;
                
                if (receveidControl.value !== otherControlValue) {
                    return { 'no_match':  message };
                }
                return null;
            }
            return { 'form_group_not_finded': message };
        };
    }
}